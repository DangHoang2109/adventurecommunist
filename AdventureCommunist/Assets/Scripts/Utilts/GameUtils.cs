﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
public enum BoostingFormular
{
    SUM, //CỘNG
    SUB, //TRỪ
    TIMES, //NHÂN
    DIV, //CHIA
}
public static class GameUtils
{
    public const long QUADRILLION =  1000000000000000; 
    public const long QUADRILLION_DIVIDE_10 =  QUADRILLION / 10; 
    public const long TRILLION =  1000000000000; 
    public const long TRILLION_DIVIDE_10 =  TRILLION / 10; 
    public const long BILLION =   1000000000; 
    public const long BILLION_DIVIDE_10 =   BILLION / 10; 
    public const long MILLION =   1000000; 
    public const long MILLION_DIVIDE_10 =   MILLION / 10; 
    public const long THOUSAND =  1000; 
    public const long THOUSAND_DIVIDE_10 =  THOUSAND / 10; 
    
    // 1000 1000000 1000000000 1000000000000
    public static readonly string[] DIGIT_LETTERS = { "K", "M", "B", "T", "P" };
    
    public static IList<T> Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        return list;
    }
    
    public static T GetRandom<T>(this T[] array)
    {
        if (array == null || array.Length == 0)
            return default;

        return array[UnityEngine.Random.Range(0, array.Length)];
    }
    public static T Last<T>(this IList<T> array)
    {
        if (array == null || array.Count == 0)
            return default;

        return array[array.Count - 1];
    }

    public static T GetRandom<T>(this List<T> array)
    {
        if (array == null || array.Count == 0)
            return default;

        return array[UnityEngine.Random.Range(0, array.Count)];
    }

    public static string[] IndexToMagnitude =
    {
       "",
       "K",
       "M",
       "B",
       "T"
    };

    public static double GetDoubleValueFromIddleUnit(this string value)
    {
        try
        {
            if (value.Length <= 1 || int.TryParse(value.Substring(value.Length - 1), out int _))
                return double.Parse(value);

            int AmountLetters = 24; //alphabet have 24 letter
            double Thousand = 1000;

            double doubleValue = 0;
            if (value.Length > 2)
            {
                string last2 = value.Substring(value.Length - 2);
                string valueRemain = value.Substring(0, value.Length - 2);
                //check chữ cái đầu tiên là chữ hay số
                string first = last2.Substring(0, 1);
                string last = last2.Substring(1);

                //nếu second là số =? đây là 1 số bình thường
                if (int.TryParse(last, out int _))
                    return double.Parse(value);

                //nếu first là số => tìm xem last có trong indexToMagnitude không

                if (int.TryParse(first, out int _))
                {
                    int index = IndexToMagnitude.ToList().IndexOf(last);

                    double.TryParse((valueRemain + first), out doubleValue);
                    return doubleValue * Thousand.Pow(index);
                }
                //first không là số => 2 kí tự này là format AB, AC...
                else
                {
                    int secondUnit = (int)(last.ToCharArray()[0]) - 65;
                    int firstUnit = (int)(first.ToCharArray()[0]) - 65;

                    int unitInt = firstUnit * AmountLetters + secondUnit + IndexToMagnitude.Length;
                    double.TryParse((valueRemain), out doubleValue);

                    return doubleValue * Thousand.Pow(unitInt);
                }
            }
            else if (value.Length == 2)
            {
                //check chữ cái cuối nằm đâu trong index
                string last = value.Substring(1);
                string valueRemain = value.Substring(0, value.Length - 1);

                if (!int.TryParse(last, out int _))
                {
                    int index = IndexToMagnitude.ToList().IndexOf(last);

                    double.TryParse((valueRemain), out doubleValue);
                    return doubleValue * Thousand.Pow(index);
                }
                else
                    return double.Parse(value);
            }
            else
                return double.Parse(value);
        }
        catch (Exception e)
        {
            Debug.LogError($"Erorr when casting {value}");
            Debug.LogError(e.StackTrace);

            return 0;
            //throw e;
        }

    }

    [UnityEditor.MenuItem("Tool/Test Reverse Iddle Unit")]
    public static string FormatReverseValueToIddleUnit()
    {
        string inputValue = "0" ;

        double result = inputValue.GetDoubleValueFromIddleUnit();
        Debug.Log("Result " + result);

        Debug.Log("Cast Back " + result.ToIddleUnitString());
        return result.ToString();
    }

    public static string ToIddleUnitString(this double value, double belowValueWillFormatDot = 100000)
    {
        try
        {
            if (value is double.PositiveInfinity || value is double.NegativeInfinity)
            {
                Debug.LogError("value is infinity");
                return "";
            }
            //round bỏ phần thập phân lẻ đi
            value = RoundDownValue(value);
            ///Game gốc hoặc các dòng iddle thì giá trị dưới 100k vẫn ghi dạng 50.200
            ///TỪ 100k trở lên sẽ group giá trị, lấy 2 chữ số thập phân như 120.25K, 1.25B... 
            if (value < belowValueWillFormatDot)
                return FormatMoneyDot((long)value);

            int TenCubed = 1000;
            value = Math.Abs(value);

            //Số lần 1000
            int magnitude = 0;
            while (value >= TenCubed)
            {
                value /= TenCubed;
                magnitude += 1;
            }

            string unit;
            int AmountLetters = 24; //alphabet have 24 letter

            if (magnitude < IndexToMagnitude.Length)
                unit = IndexToMagnitude[magnitude];
            else
            {
                var unitInt = magnitude - IndexToMagnitude.Length;
                int secondUnit = unitInt % AmountLetters;
                int firstUnit = unitInt / AmountLetters;

                //ascii code of A is 65
                unit = ((char)(firstUnit + 65)).ToString() + ((char)(secondUnit + 65)).ToString();
            }
            return value.ToString("0.##") + unit;
        }
        catch (Exception e)
        {
            Debug.LogError($"Erorr when casting {value}");
            Debug.LogError(e.StackTrace);

            return "0";
            //throw e;
        }

    }
    public static double RoundValue(double value)
    {
        return System.Math.Round(value);
    }
    public static double RoundDownValue(double value)
    {
        return System.Math.Floor(value);
    }

    [UnityEditor.MenuItem("Tool/Test Iddle Unit")]
    public static string FormatValueToIddleUnit()
    {
        double inputValue = double.MaxValue;

        string result = inputValue.ToIddleUnitString();
        Debug.Log("Result " + result);

        return result.ToString();
    }

    public static double Pow(this double r, double exponent)
    {
        //dont use the damn Mathf.Pow, it return float and can holding big value of double
        return System.Math.Pow(r, exponent);
    }

    /// <summary>
    /// Lấy tổng giá tiền mua X item theo công thức lũy tiến IDDLE
    /// </summary>
    /// <param name="basePrice">initial value, giá khởi đầu</param>
    /// <param name="multiplierRate">phần nhân multiplier</param>
    /// <param name="amountToBuy">lượng cần mua</param>
    /// <param name="amountOwned">lượng đã có</param>
    /// <returns>Tổng tiền mua amountToBuy item</returns>
    public static double GetCostBuyByIddleFormular(
        double basePrice,
        double multiplierRate,
        int amountToBuy,
        int amountOwned)
    {
        return basePrice * multiplierRate.Pow(amountOwned) * (multiplierRate.Pow(amountToBuy) - 1) / (multiplierRate - 1);
    }

    /// <summary>
    /// Lấy ra tổng số item có thể mua với lượng tiền hiện tại
    /// </summary>
    /// <param name="currentWalletCoin">lượng coin user có</param>
    /// <param name="basePrice">initial value, giá base</param>
    /// <param name="multiplierRate">hệ số nhân</param>
    /// <param name="amountOwned">lượng item đã có</param>
    /// <returns></returns>
    public static int GetMaxItemCanBuyByIddleFormular(
        double currentWalletCoin,
        double basePrice,
        double multiplierRate,
        int amountOwned
        )
    {

        return Mathf.FloorToInt(
            (float)System.Math.Log(

                ((currentWalletCoin * (multiplierRate - 1) / (basePrice * multiplierRate.Pow(amountOwned))) + 1)
                ,
                multiplierRate
            )
        );
    }
    /// <summary>
    /// it is not really correctly (the sub value) but it works reliably, far low risk than the former function <br></br>
    /// WARNING! Not parse negative number  (it is not hard but I just don't like to do it :D )
    /// </summary>
    public static string FormatMoney(long num)
    {
       
        long pre, sub;
        string key;

        // if else looks ridiculous but it's performance is sure better than conversion to string
        //      then count then extract string, and it is not reliably if you convert back to the number 
        if (num >= QUADRILLION)
        {
            pre = num / QUADRILLION;
            sub = (num % QUADRILLION) / QUADRILLION_DIVIDE_10;
            key = DIGIT_LETTERS[4];
        }
        else if (num >= TRILLION)
        {
            pre = num / TRILLION;
            sub = (num % TRILLION) / TRILLION_DIVIDE_10;
            key = DIGIT_LETTERS[3];
        }
        else if (num >= BILLION)
        {
            pre = num / BILLION;
            sub = (num % BILLION) / BILLION_DIVIDE_10;
            key = DIGIT_LETTERS[2];
        }
        else if (num >= MILLION)
        {
            pre = num / MILLION;
            sub = (num % MILLION) / MILLION_DIVIDE_10;
            key = DIGIT_LETTERS[1];
        }
        else if (num >= THOUSAND)
        {
            pre = num / THOUSAND;
            sub = (num % THOUSAND) / THOUSAND_DIVIDE_10;
            key = DIGIT_LETTERS[0];
        }
        else
        {
            pre = num;
            sub = 0;
            key = string.Empty;
        }

        if (sub == 0)
        {
            return pre.ToString() + key;
        }
        else
        {
            return pre.ToString() + $".{sub}{key}";
        }
    }
    public static string FormatMoneyDot(double money, string separator = ".")
    {
        bool isNegative = false;
        if (money < 0)
        {
            money = -money;
            isNegative = true;
        }

        if (money == 0)
            return "0";

        string result = money.ToString();
        int index = result.Length - 1;
        int split = 0;
        while (index > 0)
        {
            split++;
            if (split % 3 == 0)
            {
                result = result.Insert(index, "" + separator);
                split = 0;
            }
            index--;
        }
        if (isNegative)
            result = "-" + result;
        return result;
    }
    public static long GetCurrentTimeSeconds()
    {
        Int64 unixTimestamp = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        return (long)unixTimestamp;
    }

    public static string ConvertFloatToTime(double second, string format = "hh':'mm':'ss")
    {
        double totalSeconds = second;
        TimeSpan time = TimeSpan.FromSeconds(totalSeconds);

       
        return time.ToString(format);
    }
    public static TimeSpan ConvertFloatToTimeSpan(double second)
    {
        double totalSeconds = second;
        TimeSpan time = TimeSpan.FromSeconds(totalSeconds);


        return time;
    }
    public static DateTime UnixTimeToDateTime(long unixtime)
    {
        DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixtime);
        return dtDateTime;
    }
    public static long DateTimeToUnixTime(DateTime time)
    {
        DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        long unixTimeStampInTicks = (time.ToUniversalTime() - unixStart).Ticks;
        return (long)unixTimeStampInTicks / TimeSpan.TicksPerSecond;
    }

    #region Number
    public static long RoundUpValue(long value, long divder)
    {
        long addOn = (value % divder == 0 ? 0 : divder - value % divder);
        return value + addOn;
    }
    public static long RoundUpValue(long value, int places)
    {
        long bound = (long)Mathf.Pow(10, places);

        //check nếu value không lớn hơn hệ số mũ places => lỗi, làm tròn tới places gần nhất
        long addOn = (value % bound == 0 ? 0 : bound - value % bound);
        return value + addOn;
    }
    public static int RoundUpValue(int value, int places)
    {
        int bound = (int)Mathf.Pow(10, places);

        //check nếu value không lớn hơn hệ số mũ places => lỗi, làm tròn tới places gần nhất
        int addOn = (value % bound == 0 ? 0 : bound - value % bound);
        return value + addOn;
    }
    //Làm tròn, lên hay xuống tùy theo phần dư
    public static long RoundValue(long value, int places)
    {
        long bound = (long)Mathf.Pow(10, places);

        long exceed = value % bound;

        if (exceed == 0) return value;

        if (exceed < bound / 2)
            return value - exceed;

        if (exceed > bound / 2)
            return value + (bound - exceed);

        return value;
    }
    /// <summary>
    /// Alway random no matter a < b or b < a
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static int SafeRandom(int a, int b, bool isExcludeMax = true)
    {
        if (a == b)
            return a;
        else if (a < b)
            return UnityEngine.Random.Range(a, isExcludeMax ? b : b + 1);
        else
            return UnityEngine.Random.Range(b, isExcludeMax ? a : a + 1);
    }

    public static long RoundValue(float value, int places)
    {
        long longConverted = (long)value;
        //convert về long gần nhất rồi round như long
        float frac = value - longConverted;

        if (frac >= 0.5) longConverted++;

        return RoundValue(longConverted, places);
    }

    public static long RoundDownValue(long value, int places)
    {
        long bound = (long)Mathf.Pow(10, places);

        //check nếu value không lớn hơn hệ số mũ places => lỗi, làm tròn tới places gần nhất
        long addOn = (value % bound == 0 ? 0 : value % bound);
        return value - addOn;
    }
    public static double RoundDownValue(double value, int places)
    {
        double bound = (double)Mathf.Pow(10, places);

        //check nếu value không lớn hơn hệ số mũ places => lỗi, làm tròn tới places gần nhất
        double addOn = (value % bound == 0 ? 0 : value % bound);
        return value - addOn;
    }
    #endregion

    public static float ConvertLongToFloat(long value)
    {
        float result = 0f;
        if (value > float.MaxValue)
        {
            result = float.MaxValue;
        }
        else
        {
            result = (float) value;
        }
        return result;
    }
    public static int ConvertLongToInt(long value)
    {
        int result = 0;
        if (value > int.MaxValue)
        {
            result = int.MaxValue;
        }
        else
        {
            result = (int) value;
        }
        return result;
    }


    public static bool CheckInstallSource()
    {
        string installSource = Application.installerName;
        if (!installSource.Contains("com.android.vending")) {
            //show dialog thông báo và click ok thì mở tới playstore của game.
            //Cài ở store ngoài
            return false;
        }

        return true;
    }

//    public static void DebugInfo(this CardData _card)
//    {
//        string data = $"ID {_card.cardID} Name {_card.CardUsageConfig.CardConfig.name} level {_card.currentLevel} have state {_card.state}, new? {_card.isNew}, " +
//$"\n card owned {_card.CurrentCardOwned}, card require {_card.NextCardRequired}, costUpgrade {_card.CostUpgradeNextLevel}. \n";
//        ;

//        Debug.Log(data);
//    }

    public static double CalculateWithBoostingFormular(double baseValue , double addValue, BoostingFormular formular)
    {
        switch (formular)
        {
            case BoostingFormular.SUM:
                baseValue += addValue;
                break;
            case BoostingFormular.SUB:
                baseValue -= addValue;
                break;
            case BoostingFormular.TIMES:
                baseValue *= addValue;
                break;
            case BoostingFormular.DIV:
                baseValue /= addValue;
                break;
        }
        return baseValue;
    }

#if UNITY_EDITOR
    public static List<T> ParseEnumToList<T>()
    {
        return Enum.GetValues(typeof(T))
                          .Cast<T>()
                          .ToList<T>();
    }
    public static List<String> LoadAllNameFolderInFolder(string path)
    {
        if (path != "")
        {
            if (path.EndsWith("/"))
            {
                path = path.TrimEnd('/');
            }
        }
        List<GameObject> results = new List<GameObject>();


        DirectoryInfo dirInfo = new DirectoryInfo(path);

        DirectoryInfo[] folders = dirInfo.GetDirectories();
        if (folders.Length > 0)
        {
            List<string> _allFilePaths = new List<string>();
            foreach (DirectoryInfo folder in folders)
            {
                _allFilePaths.Add(folder.Name);
            }
            return _allFilePaths;
        }

        return null;
    }

    public static List<T> LoadAllAssetsInFolder<T>(string path, List<string> patterns) where T : UnityEngine.Object
    {
        if (path != "")
        {
            if (path.EndsWith("/"))
            {
                path = path.TrimEnd('/');
            }
        }
        List<T> results = new List<T>();


        DirectoryInfo dirInfo = new DirectoryInfo(path);

        DirectoryInfo[] folders = dirInfo.GetDirectories();
        if (folders.Length > 0)
        {
            List<string> _allFilePaths = new List<string>();
            foreach (DirectoryInfo folder in folders)
            {
                foreach (var item in patterns)
                {
                    string[] _temp = Directory.GetFiles(folder.FullName, item, SearchOption.AllDirectories);
                    _allFilePaths.AddRange(_temp);
                }
            }

            foreach (var item in _allFilePaths)
            {
                string fullPath = item.Replace(@"\", "/");
                string assetPath = "Assets" + fullPath.Replace(Application.dataPath, "");

                T asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath) as T;

                if (asset != null)
                {
                    results.Add(asset);
                }
            }
        }
        else
        {
            List<string> _allFilePaths = new List<string>();
            foreach (var item in patterns)
            {
                string[] _temp = Directory.GetFiles(path, item, SearchOption.AllDirectories);
                _allFilePaths.AddRange(_temp);
            }
            foreach (var item in _allFilePaths)
            {
                string fullPath = item.Replace(@"\", "/");
                //string assetPath = "Assets" + fullPath.Replace(Application.dataPath, "");

                T asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(fullPath) as T;

                if (asset != null)
                {
                    results.Add(asset);
                }
            }
        }

        return results;
    }
    public static T LoadAssetInFolder<T>(string path, string extention) where T : UnityEngine.Object
    {
        string fullPath = path + extention;
        T results = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(fullPath);

        return results;
    }

    public static List<T> LoadAllAssetInFolder<T>(string path, string extention) where T : UnityEngine.Object
    {
        List<T> results = new List<T>();

        string[] temps = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
        foreach(string t in temps)
        {
           
            T result = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(t);
            if (result != null)
            {

                results.Add(result);
            }
        }

        return results;
    }

#endif
}
