﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class CallbackEventObject : UnityEvent<object>
{

}
public class CallbackEventInt : UnityEvent<int>
{

}
public class CallbackEventLong : UnityEvent<long>
{

}
public class CallbackEventDouble : UnityEvent<double>
{

}
public class CallbackEventBooster : UnityEvent<BoosterCommodity>
{

}
public class UserProfile : MonoSingleton<UserProfile>
{
    private Dictionary<BoosterType, CallbackEventBooster> boostes;
    private Dictionary<string, CallbackEventBooster> callbackProfiles;


    public override void Init()
    {
        base.Init();
        this.boostes = new Dictionary<BoosterType, CallbackEventBooster>();
        this.callbackProfiles = new Dictionary<string, CallbackEventBooster>();
    }

    
    
    #region CALLBACK BOOSTER
    public void AddCallbackBooster(BoosterType type, UnityAction<BoosterCommodity> callback)
    {
        if (this.boostes.ContainsKey(type))
        {
            this.boostes[type].AddListener(callback);
        }
        else
        {
            CallbackEventBooster unityEvent = new CallbackEventBooster();
            unityEvent.AddListener(callback);
            this.boostes.Add(type, unityEvent);
        }
        BoosterCommodity booster = UserBoosters.Instance.GetBoosterCommodity(type);
        if (booster!=null)
            this.ChangeBoosterValue(booster.type, booster);
    }

    public void RemoveCallbackBooster(BoosterType type, UnityAction<BoosterCommodity> callback)
    {
        if (this.boostes.ContainsKey(type))
        {
            this.boostes[type].RemoveListener(callback);
        }
    }
    // invoke callbacks
    public void ChangeBoosterValue(BoosterType type, BoosterCommodity booster)
    {
        if (this.boostes.ContainsKey(type))
        {
            this.boostes[type].Invoke(booster);
        }
    }
    #endregion
    
     #region BOOSTER


     public BoosterCommodity SetBooster(BoosterType type, long value, string where, int level = 1)
     {
         BoosterCommodity commodity = UserBoosters.Instance.SetValueBooster(type, value);
         if (commodity != null)
         {
             this.ChangeBoosterValue(commodity.type, commodity);
             
         }
         return commodity;
     }
     /// <summary>
     /// Source: booster nào đó
     /// </summary>
     /// <param name="booster">Loại booster</param>
     /// <param name="where">Vị trí add(wheel,game, ...)</param>
     /// <param name="level"></param>
     /// <returns></returns>
    public BoosterCommodity AddBooster(BoosterCommodity booster, string from, string where,bool isCallback = true, int level = 0)
    {
        BoosterCommodity commodity = UserBoosters.Instance.AddValueBooster(booster);
        if (commodity != null)
        {
            if (isCallback)
            {
                this.ChangeBoosterValue(commodity.type, commodity);
            }
            
        }
        return commodity;
    }
     /// <summary>
     /// Source: booster nào đó
     /// </summary>
     /// <param name="type">Loại booster</param>
     /// <param name="value">Gia tri booster</param>
     /// <param name="where">Vị trí add(wheel,game, ...)</param>
     /// <param name="level"></param>
     /// <returns></returns>
    public BoosterCommodity AddBooster(BoosterType type, long value, string from, string where, bool isCallback = true, int level = 1)
    {
        BoosterCommodity commodity = UserBoosters.Instance.AddValueBooster(type, value);
        if (commodity != null)
        {
            if (isCallback)
            {
                this.ChangeBoosterValue(commodity.type, commodity);
            }
        }
        return commodity;
    }
     /// <summary>
     /// Source: booster nào đó
     /// </summary>
     /// <param name="boosters">Loại booster</param>
     /// <param name="where">Vị trí add(wheel,game, ...)</param>
     /// <param name="level"></param>
     /// <returns></returns>
    public List<BoosterCommodity> AddBoosters(List<BoosterCommodity> boosters, string from, string where, bool isCallback = true, int level = 1)
    {
        List<BoosterCommodity> temps = new List<BoosterCommodity>();
        foreach (var b in boosters)
        {
            temps.Add(this.AddBooster(b, from, where, isCallback, level));
        }

        return temps;
    }
     /// <summary>
     /// SINK: booster nào đó
     /// </summary>
     /// <param name="type">Loại booster</param>
     /// <param name="value">Gia tri booster</param>
     /// <param name="where">Vị trí use(wheel,game, ...)</param>
     /// <param name="level"></param>
     /// <returns></returns>
    public bool UseBooster(BoosterType type, long value, string from, string where,bool isCallback = true, int level = 1)
    {
        BoosterCommodity commodity = UserBoosters.Instance.UseBooster(type, value);
        if (commodity != null)
        {
            if (isCallback)
            {
                this.ChangeBoosterValue(commodity.type, commodity);
            }
            return true;
        }
        return false;
    }

     public bool UseBooster(BoosterCommodity booster, string from, string where, bool isCallback = true, int level = 1)
     {
         return this.UseBooster(booster.type, booster.GetValue(), from, where, isCallback, level);
     }
     /// <summary>
     /// Source: booster nào đó
     /// </summary>
     /// <param name="boosters">Loại booster</param>
     /// <param name="where">Vị trí add(wheel,game, ...)</param>
     /// <param name="level"></param>
     /// <returns></returns>
    public bool UseBoosters(List<BoosterCommodity> boosters,string from, string where, bool isCallback = true, int level = 1)
    {
        if (this.IsCanUseBoosters(boosters))
        {
            foreach (BoosterCommodity booster in boosters)
            {
                this.UseBooster(booster.type, booster.GetValue(), from, where, isCallback, level);
            }
            return true;
        }
        return false;
    }

    public bool IsCanUseBooster(BoosterCommodity booster)
    {
        return IsCanUseBooster(booster.type, booster.GetValue());
    }

    public bool IsCanUseBooster(BoosterType type, long value)
    {
        return UserBoosters.Instance.IsHasBooster(type, value);
    }
    public bool IsCanUseBoosters(List<BoosterCommodity> boosters)
    {
        foreach (BoosterCommodity booster in boosters)
        {
            if (!this.IsCanUseBooster(booster.type, booster.GetValue()))
            {
                return false;
            }
        }
        return true;
    }

    #endregion
}