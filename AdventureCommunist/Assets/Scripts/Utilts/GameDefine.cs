public static class GameDefine
{
    public const string USER_BOOSTER_DATA = "USER_BOOSTER_DATA";
    public const string USER_INFO_DATA = "USER_INFO_DATA";
    public const string USER_RATED = "USER_RATED"; //user đã rate (1-4 star hoặc 5 star) sẽ không hiện rate lại nữa

    public const long COMRADE_DEFAULT = 0;

    public const long GOLD_DEFAULT = 50;

    
    public const double TIME_TOTAL_A_DAY = 86400; //tổng số giây của 24h

    // TODO! Mr Hoang
    public const long RATE_CASH_TO_COIN = 1;
    public const long RATE_DOLLAR_TO_CASH = 1;

    public const long RATE_DOLLAR_TO_COIN = RATE_CASH_TO_COIN * RATE_DOLLAR_TO_CASH;
}


/// <summary>
/// Thứ tự layer được sắp xếp từ thấp đến cao
/// </summary>
public static class LayerName
{
    public const string Scene = "Scene";
    public const string Game = "Game";
    public const string Popup = "Popup";
    public const string Map = "Map";
}

public static class TagName
{
    public const string Tile = "Tile";
    public const string DragObject = "DragObject";
    public const string RewardObject = "RewardObject";
}

public static class SceneName
{
    public const string LOADING = "LoadingScene";
    public const string HOME = "GameScene";
    
    // TODO! rename!
    public const string GAME = "GameScene";

    public const string MAIN_GAME = "GameScene";
}