﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#pragma warning disable 618

[System.Serializable]
public class UserBoosters
{
    public List<BoosterCommodity> boosters;

    private static UserBoosters instance;

    public static UserBoosters Instance
    {
        get
        {
            if(instance == null)
            {
                instance = GameDataManager.Instance.Boosters;
            }
            return instance;
        }
    }

    public static UserBoosters Create()
    {
        return new UserBoosters()
        {
            boosters = new List<BoosterCommodity>()
        };
    }
    
    public UserBoosters()
    {
        this.boosters = new List<BoosterCommodity>();
    }
    /// <summary>
    /// Create new user
    /// </summary>
    public void CreateUser()
    {
        this.AddBooster(BoosterType.GOLD, GameDefine.GOLD_DEFAULT);
    }

    public void AddBooster(BoosterType type,long value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if(b == null)
        {
            this.boosters.Add(new BoosterCommodity(type, value));
            this.Save();
        }
    }
    public BoosterCommodity AddValueBooster(BoosterType type, long value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            b.Add(value);
            this.Save();
            return b;
        }
        return null;
    }
    public BoosterCommodity AddValueBooster(BoosterCommodity booster)
    {
        BoosterCommodity b = this.GetBoosterCommodity(booster.type);
        if (b != null)
        {
            b.Add(booster.GetValue());
            this.Save();
            return b;
        }
        return null;
    }
    public BoosterCommodity SetValueBooster(BoosterType type, long value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            b.Set(value);
            this.Save();
            return b;
        }
        return null;
    }
    public BoosterCommodity UseBooster(BoosterType type, long value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            if (b.Use(value))
            {
                this.Save();

                return b;
            }
        }
        return null;
    }
    public bool IsHasBooster(BoosterType b)
    {
        return this.boosters.Find(x=>x.type == b) != null;
    }
    public bool IsHasBooster(BoosterType type, long value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            return b.CanUse(value);
        }
        return false;
    }
    public BoosterCommodity GetBoosterCommodity(BoosterType type)
    {
        return this.boosters.Find(x=>x.type == type);
    }

    public void Save()
    {
        GameDataManager.Instance.SaveBoosterData();
    }
}
public enum BoosterType
{
    NONE = -1,

    COMRADE = 0,
    SCIENCE = 1,
    GOLD = 2,
    TIME_SKIP = 3,
    CARDS = 4,
    CHEST = 5,

    I1 = 100,
    I1T1 = 101,
    I1T2 = 102,
    I1T3 = 103,
    I1T4 = 104, 
    I1T5 = 105,
    I1T6 = 106,
    I1T7 = 107,
    I1T8 = 108,
    I1T9 = 109,
    I1T10 = 110,
    I1T11 = 111,
    I1T12 = 112,

    I2 = 200,
    I2T1 = 201,
    I2T2 = 202,
    I2T3 = 203,
    I2T4 = 204,
    I2T5 = 205,
    I2T6 = 206,
    I2T7 = 207,
    I2T8 = 208,
    I2T9 = 209,
    I2T10 = 210,
    I2T11 = 211,
    I2T12 = 212,

    I3 = 300,
    I3T1 = 301,
    I3T2 = 302,
    I3T3 = 303,
    I3T4 = 304,
    I3T5 = 305,
    I3T6 = 306,
    I3T7 = 307,
    I3T8 = 308,
    I3T9 = 309,
    I3T10 = 310,
    I3T11 = 311,
    I3T12 = 312,

    I4 = 400,
    I4T1 = 401,
    I4T2 = 402,
    I4T3 = 403,
    I4T4 = 404,
    I4T5 = 405,
    I4T6 = 406,
    I4T7 = 407,
    I4T8 = 408,
    I4T9 = 409,
    I4T10 = 410,
    I4T11 = 411,
    I4T12 = 412,

    I5 = 500,
    I5T1 = 501,
    I5T2 = 502,
    I5T3 = 503,
    I5T4 = 504,
    I5T5 = 505,
    I5T6 = 506,
    I5T7 = 507,
    I5T8 = 508,
    I5T9 = 509,
    I5T10 = 510,
    I5T11 = 511,
    I5T12 = 512,

    ///// <summary>
    ///// ID is E-I-XX,
    ///// first is for event id, if id < 1000 => for main game motherland
    ///// second is for industry index + 1, start from 1
    ///// last 2 is the index
    ///// </summary>

    ////Motherland - Potatoes
    //Farmers = 101,
    //Communes = 102,
    //Freights = 103,
    //Plantations = 104,
    //Irrigations = 105,
    //Greenhouses = 106,
    //Barges = 107,
    //ColdStorages = 108,
    //CropDusters = 109,
    //Biodomes = 110,
    //SkyFarms = 111

}


[System.Serializable]
public class BoosterCommodity
{ 
    public BoosterType type;
    [System.Obsolete("Use GetValue() instead")]
    public string value;

    public double Value => this.GetValueDouble();
    public string ValueIddleUnit => Value.ToIddleUnitString();
    public string GetIddleUnitString => this.GetValueDouble().ToIddleUnitString();

    public BoosterCommodity()
    {
        this.type = BoosterType.NONE;
        this.value = "0";// why not set constant? cuz constant can be decoded easily
    }
    public BoosterCommodity(BoosterCommodity c)
    {
        this.type = c.type;
        this.value = c.value;
    }
    public BoosterCommodity(BoosterType key, double value)
    {
        this.type = key;
        this.value = (value).ToString();
    }

    public BoosterCommodity(BoosterType key, double value, int id)
    {
        this.type = key;
        this.value = (value).ToString();
    }
    public BoosterCommodity Set(double value)
    {
        this.value = (value).ToString();
        return this;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    public void Add(double value)
    {
        value += double.Parse(this.value);
        if (value < 0) value = 0;
        this.value = (value).ToString();
    }

    public void Add(string otherBoosterValue)
    {
        long v = long.Parse(otherBoosterValue) + long.Parse(this.value);
        if (v < 0) v = 0;
        this.value = v.ToString();
    }

    public bool Use(double value)
    {
        double v = double.Parse(this.value);
        if (v >= value)
        {
            v -= value;
            if (v < 0) v = 0;
            this.value = (v).ToString();
            return true;
        }
        return false;
    }
    public bool CanUse(double value)
    {
        double v = double.Parse(this.value);
        return v >= value;
    }

    public double GetValueDouble()
    {
        return this.value.GetDoubleValueFromIddleUnit();
    }
    public long GetValue()
    {
        try
        {
            return long.Parse(this.value);
        }
        catch (Exception e)
        {
            Debug.LogError("VALUE: " + this.value);
            Debug.LogError(e);
        }

        return 0;
    }
}


[System.Serializable]
public class ChestCommodity
{
    public BoosterType Type => BoosterType.CHEST;
    public ChestID _chestID;
    public int _value;

    public BoosterCommodity CastToBooster => new BoosterCommodity(Type, _value);

    public ChestID ChestID => _chestID;


}
