﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerModel
{
    public UserInfo info;

    public static PlayerModel Create()
    {
#pragma warning disable CS0618
        return new PlayerModel()
#pragma warning restore CS0618
        {
            info = UserInfo.Create()
        };
    }
    
    [System.Obsolete("Use PlayerModel.Create instead")]
    public PlayerModel()
    {
        
    }

    public PlayerModel(PlayerModel p)
    {
        this.info = p.info;
    }
}
