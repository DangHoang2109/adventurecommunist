﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextResize : MonoBehaviour
{
    public TextMeshProUGUI tmpValue;
    public RectTransform panelResize;
    public Rect anchor;
#if UNITY_EDITOR
    protected void OnValidate()
    {
        this.tmpValue = this.GetComponentInChildren<TextMeshProUGUI>();
        this.AutoResize();
        
    }
#endif

    public void UpdateText(string msg = "")
    {
        if (!string.IsNullOrEmpty(msg))
        {
            this.tmpValue.text = msg;
        }
        this.AutoResize();
    }

    public void AutoResize()
    {
        
        this.panelResize.sizeDelta = new Vector2(this.tmpValue.preferredWidth + this.anchor.x, this.tmpValue.preferredHeight + this.anchor.y);

    }
}
