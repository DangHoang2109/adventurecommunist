﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Configs/GameAssetsConfigs", fileName = "GameAssetsConfigs")]
public class GameAssetsConfigs : ScriptableObject
{

    public static GameAssetsConfigs Instance
    {
        get
        {
            return LoaderUtility.Instance.GetAsset<GameAssetsConfigs>("Home/Configs/GameAssetsConfigs");
        }
    }

    public List<EventAssetConfig> _eventAssetConfig;
    public MissionConfigAssets _missionAssets;

    public EventAssetConfig GetEventAssetConfig(EventID _id)
    {
        return _eventAssetConfig.Find(x => x._eventID == _id);
    }

    #region Editor
    [ContextMenu("InputMissionAsset")]
    private void Editor_InputMissionAsset()
    {
        List<MissionConfigAsset> configs = new List<MissionConfigAsset>()
        {
            new MissionConfigAsset()
            {
                 _name = "Break Ground!",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "First Draft!",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "Health Before Wealth!",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "Re-Search And Destroy",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I5,
            },


            new MissionConfigAsset()
            {
                 _name = "More Potatoes, More Glory!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1,
            },
            new MissionConfigAsset()
            {
                 _name = "Salt of the Earth!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T1,
            },
            new MissionConfigAsset()
            {
                 _name = "Work Smarter! And Harder!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T2,
            },
            new MissionConfigAsset()
            {
                 _name = "A Miner Advantage!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T12,
            },
            new MissionConfigAsset()
            {
                 _name = "Land in the Hand!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T12,
            },
            new MissionConfigAsset()
            {
                 _name = "Stronger Than Dirt",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "Armed and Glorious!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "A Healthy People!",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T12,
            },

            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1,
            },
            new MissionConfigAsset()
            {
                 _name = "Salt of the Earth!",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "Work Smarter! And Harder!",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "A Miner Advantage!",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "Army Of One!",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5,
            },
            new MissionConfigAsset()
            {
                 _name = "First Aid",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I5,
            },

            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect Cards",
                  _requirementResource = BoosterType.CARDS,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect Science",
                  _requirementResource = BoosterType.SCIENCE,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.SPEND_SCIENCE,
                  _requirementStringFormat = "Spend science",
                  _requirementResource = BoosterType.SCIENCE,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.UPGRADE_CARDS,
                  _requirementStringFormat = "Upgrade Cards",
                  _requirementResource = BoosterType.CARDS,
            }
        };
        if(_missionAssets == null)
            _missionAssets = new MissionConfigAssets();

        this._missionAssets.configs = configs;
    }


    #endregion
}


[System.Serializable]
public class BoosterCommonAssetConfig
{
    public BoosterType _boosterID;

    public Sprite _iconBooster;
    public Color _colorBooster;
}
[System.Serializable]
public class EventAssetConfig
{
    public EventID _eventID;

    public List<BoosterCommonAssetConfig> _asset;
    
    public BoosterCommonAssetConfig GetAsset(BoosterType _type)
    {
        return this._asset.Find(x => x._boosterID == _type);
    }
}
[System.Serializable]
public class MissionConfigAssets
{
    
    public static MissionConfigAssets Instance => GameAssetsConfigs.Instance._missionAssets;

    public List<MissionConfigAsset> configs;

    public MissionConfigAsset GetConfig(MissionRequirementType _type)
    {
        return this.configs.Find(x => x._requirementType == _type);
    }
    public MissionConfigAsset GetConfig(MissionRequirementType _type, BoosterType _resource)
    {
        return this.configs.Find(x => x._requirementType == _type && x._requirementResource == _resource);
    }
    public Sprite GetSprite(MissionRequirementType _type)
    {
        return GetConfig(_type)?._spr;
    }
    public string GetRequirementFormat(MissionRequirementType _type)
    {
        return GetConfig(_type)?._requirementStringFormat;
    }
    public string GetMissionName(MissionRequirementType _type, BoosterType _resource)
    {
        return GetConfig(_type, _resource)?._name;
    }
}
[System.Serializable]
public class MissionConfigAsset
{
    public MissionRequirementType _requirementType;
    public BoosterType _requirementResource;

    public Sprite _spr;
    public string _requirementStringFormat;
    public string _name;
    
}