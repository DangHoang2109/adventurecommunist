﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cosina.Components;
using System.Linq;
using Random = UnityEngine.Random;

[System.Serializable]
public class UserDatas
{
    public UserInfo info;
    
    public static UserDatas Instance
    {
        get { return GameDataManager.Instance.GameDatas.userDatas; }
    }

    public static UserDatas Create()
    {
#pragma warning disable CS0618
        return new UserDatas()
#pragma warning restore CS0618
        {
            info = UserInfo.Create(),
        };
    }


    [System.Obsolete("Use UserDatas.Create instead!")]
    public UserDatas()
    {
        // this.info = new UserInfo();
        // this.languages = new UserLanguageData();
    }

    public void CreateUser()
    {
    }

    public void OpenGame()
    {
        // this.winSteak.OpenGame();
        // onlineBonus.OnOpenApp();
        // adsBonus.OnOpenApp();
    }
}



[System.Serializable]
public class UserInfo
{  
   public string id;
   public string nickname;
   public string avatar;
   private Sprite sprAvatar;

   public string Avatar
   {
       get => this.avatar;
       set { 
           this.avatar = value;
           this.SprAvatar = CommonAvatar.Instance.GetAvatarById(this.avatar);
       }
   }

   public Sprite SprAvatar
   {
       get
       {
           // switch (this.loginInfo.userLoginType)
           // {
           //     case UserLoginInfo.UserLoginType.UnLogin:
           //         this.sprAvatar = CommonAvatar.Instance.GetAvatarById(this.avatar);
           //         break;
           //     case UserLoginInfo.UserLoginType.Google:
           //         //TODO parse avatar google
           //         break;
           //     case UserLoginInfo.UserLoginType.Facebook:
           //         this.sprAvatar = this.loginInfo.facebookInfo.GetAvatarFromBase64;
           //         if (this.sprAvatar == null)
           //         {
           //             this.sprAvatar = CommonAvatar.Instance.GetAvatarById(this.avatar);
           //         }
           //
           //         break;
           // }

           this.sprAvatar = CommonAvatar.Instance.GetAvatarById(this.avatar);
           return this.sprAvatar;
       }
       set
       {
           this.sprAvatar = value;
       }
   }

   public static UserInfo Create()
   {
#pragma warning disable CS0618
       return new UserInfo()
#pragma warning restore CS0618
       {
           id = string.Empty,
           nickname = $"Player_{Random.Range(0, 1000)}",
           Avatar = "0"
       };
   }
   
   [System.Obsolete("Use UserInfo.Create instead!")]
   public UserInfo()
   {
        // this.id = string.Empty;
        // this.nickname = $"Player_{Random.Range(0, 1000)}";
        // this.Avatar = "0";
   }

    public UserInfo(UserInfo i)
    {
        this.id = i.id;
        this.nickname = i.nickname;
        this.Avatar = i.avatar;
    }

    public UserInfo(string id, string name, string avatar, string skinID)
    {
        this.id = id;
        this.nickname = name;
        this.Avatar = avatar;
    }

    public void LoginFirebase(string userId)
    {
        this.id = userId;
        this.SaveData();
    }
    public void ChangeName(string name)
    {
        this.nickname = name;
        SaveData();
    }

    public void ChangeAvatar(string avatar)
    {
        this.Avatar = avatar;
        SaveData();
    }

    private void SaveData()
    {
        GameDataManager.Instance.SaveUserData();
    }
}
