﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameDatas
{
    public UserDatas userDatas;

    /// <summary>
    /// Data of iddle game, for all event currently active
    /// Chứa data của gameevent, rieng gem thì lưu trong userBoosterData vì nó là data chung
    /// Mỗi data gồm:
    ///     - GameBoosterData
    ///         + UserCoin
    ///         + UserElixir
    ///         + UserCard;
    ///         + User wheel slot spins
    ///         + User chest free, chest wait time
    ///         + Key, Crown
    ///     - GameMapData
    ///         +tileStruture
    /// </summary>
    public UserGameDatas gameDatas;

    public static GameDatas Create()
    {
#pragma warning disable CS0618
        return new GameDatas()
#pragma warning restore CS0618
        {
            userDatas = UserDatas.Create(),
            gameDatas = new UserGameDatas()
        };
    }
    
    [System.Obsolete("Use GameDatas.Create instead!")]
    public GameDatas()
    {
        this.userDatas = new UserDatas();
        gameDatas = new UserGameDatas();
    }
    
    
    /// <summary>
    /// Gọi khi user lần đầu vào game
    /// Tạo 1 user
    /// </summary>
    public void CreateUser()
    {
        this.userDatas.CreateUser();
        this.gameDatas.CreateUser();
    }
    
    /// <summary>
    /// Gọi khi mở game
    /// </summary>
    public void OpenGame()
    {
        this.userDatas.OpenGame();
        this.gameDatas.OpenGame();
    }
}
