﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class LoadingManager : MonoSingleton<LoadingManager>
{
    public static UnityAction<float> callbackProgress;
    public Image imgLoadingScene;
    public GameObject panelLoading;

    public Image imgProgress;
    public TextMeshProUGUI txtProgress;
    public TextMeshProUGUI txtTip;

    private bool isLoading;
    public bool IsLoading => isLoading;


    [Header("Next Mine")]
    public LoadingNextMine _nextMineLoader;

    public override void Init()
    {
        base.Init();
        callbackProgress += this.ProgressLoading;
    }

    private void ProgressLoading(float progress)
    {
        this.imgProgress.fillAmount = progress;
        this.txtProgress.text = string.Format("{0}%", (int)(progress * 100.0f));
    }

    public void LoadScene(bool isShow, UnityAction callback = null)
    {
        this.imgLoadingScene.gameObject.SetActive(true);
        isLoading = true;
        // if (isShow)
        // {
        //     this.txtTip.text = LanguageManager.GetString(string.Format("TIP_{0}", TipConfigs.Instance.GetRandomTipIndex()), LanguageCategory.Tips); //TipConfigs.Instance.GetRandomTip()
        // }
        float fade = isShow ? 1 : 0;
        Sequence seq = DOTween.Sequence();
        seq.Join(this.imgLoadingScene.DOFade(fade, 0.5f).SetEase(Ease.Linear));
        seq.OnComplete(() =>
        {
            if (callback != null)
            {
                callback.Invoke();
            }
            if (!isShow)
            {
                this.imgLoadingScene.gameObject.SetActive(false);
                this.imgProgress.fillAmount = 0.1f;
            }

            isLoading = false;
        });

    }
    public void ShowLoading(bool isShow, UnityAction callback = null)
    {
        isLoading = isShow;
        this.panelLoading.SetActive(isShow);
    }

    public void ShowLoadingNextMine(float timeShow, UnityAction callback = null)
    {
        isLoading = true;
        callback += () => isLoading = false;
        this._nextMineLoader.OnShow(timeShow, callback);
    }
}

[System.Serializable]
public class LoadingNextMine
{
    public CanvasGroup _canvasText;
    public GameObject _gMe;

    public void OnShow(float time = 1, UnityAction callback = null)
    {
        _gMe.SetActive(true);
        _canvasText.alpha = 1;
        _canvasText.DOFade(0f, 0.2f).SetDelay(time).OnComplete(()=> { _gMe.SetActive(false); callback?.Invoke(); });
    }

}