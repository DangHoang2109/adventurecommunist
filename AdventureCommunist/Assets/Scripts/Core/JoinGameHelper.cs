﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class JoinGameHelper : MonoSingleton<JoinGameHelper>
{
    private JoinGameDatas datas;

    public T GetJoinGameData<T>() where T : JoinGameDatas
    {
        return (T) this.datas;
    }

    public JoinGameDatas.GameType GameType
    {
        get
        {
            if (this.datas != null)
            {
                return this.datas.gameType;
            }

            return JoinGameDatas.GameType.Normal;
        }
    }



    public void JoinNormal(PlayerModel player, int level)
    {
        this.datas = new JoinGameDatas(player, JoinGameDatas.GameType.Normal, level);
        this.LoadSceneGame();
    }
    
    private void LoadSceneGame()
    {
        GameManager.Instance.OnLoadScene(SceneName.GAME);
    }
}

[System.Serializable]
public class JoinGameDatas
{
    public enum GameType
    {
        None = 0,
        Normal = 1,

        //TEST = 99,
    }
    
    public PlayerModel player;
    public GameType gameType;
    public int level;
    
    public JoinGameDatas() {}

    public JoinGameDatas(PlayerModel p, GameType type, int lvl)
    {
        this.player = p;
        this.gameType = type;
        this.level = lvl;
    }
}