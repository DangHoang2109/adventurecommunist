using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using System;
using TMPro;
public class UpgradeComradeDialog : BaseSortingDialog
{
    public TextMeshProUGUI _tmpCurrentIncomePerSec;
    [Space(10)]
    public ButtonBulkBuy _buttonBulkBuy;

    public Transform _tfSkipTimeButton;

    [Space(10)]
    public Transform _tfPanel;
    public ButtonUpgradeComrade _prefabButtonUpgrade;
    public List<ButtonUpgradeComrade> _buttonUpgrades;
    protected override void AnimationShow()
    {
        Sequence seq = DOTween.Sequence();
        seq.Join(this.panel.DOLocalMoveX(0f, this.transitionTime).SetEase(Ease.OutBack).OnComplete(this.OnCompleteShow));
        SoundManager.Instance.Play("snd_panel");
    }
    protected override void AnimationHide()
    {
        Sequence seq = DOTween.Sequence();
        seq.Join(this.panel.DOLocalMoveX(1000f, this.transitionTime).OnComplete(this.OnCompleteHide));
        SoundManager.Instance.Play("snd_panel");
    }
    public override void OnShow(object data = null, UnityAction callback = null)
    {
        base.OnShow(data, callback);

        this._buttonBulkBuy._onChangeType = this.OnClickChangeBulkType;
        ParseButtons();
        RefreshIncome();
    }

    private void OnClickChangeBulkType(BulkBuyType type)
    {
        _buttonUpgrades.ForEach(x => x.OnChangeBulkType(type));
    }
    private void RefreshIncome()
    {
        this._tmpCurrentIncomePerSec.SetText($"{IddleGameManager.Instance.CurrentBoosterData.CurrentComradeIncomePerSec}");
    }
    public void ParseButtons()
    {
        List<UserGameComradeBoosterData> _comradeDatas = IddleGameManager.Instance.CurrentBoosterData._comrades;
        if (this._buttonUpgrades == null)
            _buttonUpgrades = new List<ButtonUpgradeComrade>();

        if(_buttonUpgrades.Count < _comradeDatas.Count)
        {
            int amountSpawn = _comradeDatas.Count - _buttonUpgrades.Count;
            for (int j = 0; j < amountSpawn; j++)
            {
                ButtonUpgradeComrade _btn = Instantiate(_prefabButtonUpgrade, this._tfPanel);
                _buttonUpgrades.Add(_btn);
            }
        }
        for (int i = 0; i < _comradeDatas.Count; i++)
        {
            _buttonUpgrades[i].OnInit(_comradeDatas[i]);
            _buttonUpgrades[i]._OnClickedUpgradeSuccess += this.RefreshIncome;
        }
        _tfSkipTimeButton.SetAsLastSibling();
    }

}
public enum BulkBuyComradeType
{
    ONE = 1,
    MAX = 2
}
