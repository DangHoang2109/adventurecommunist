using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopUIManager : MonoBehaviour
{
    private static TopUIManager _instance;
    public static TopUIManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameSceneManager.Instance._topBarUI;
            return _instance;
        }
    }

    public ComradeTopHandler _comradeBooster;
    public IdleBoosterUI _scienceBooster;
    public CashBoosterUI _goldBooster;

    public void OnInit()
    {
        _comradeBooster.AssignCallback();
        _scienceBooster.AssignCallback();
        _goldBooster.AssignCallback();
    }

    private void OnDisable()
    {
        _comradeBooster.UnAssignCallback();
        _scienceBooster.UnAssignCallback();
        _goldBooster.UnAssignCallback();
    }
}
