using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class BaseButtonHandler : MonoBehaviour
{
    public Color _colorActive = Color.white;
    public Color _colorInActive = Color.grey;

    public Transform _tfPanel;
    public Image _imgPanel;

    public Button _btnClick;

    [SerializeField]
    protected Vector3 _initPosition;
    protected bool isShowing = false;
     

    [Space(10)]
    public NotiBadge _badge;
    public NotiBubble _bubble;

    public System.Action<BaseButtonHandler> OnClickShow;
    public System.Action<BaseButtonHandler> OnClickHide;

#if UNITY_EDITOR
    protected void OnValidate()
    {
        this._badge = this.GetComponentInChildren<NotiBadge>();
        this._bubble = this.GetComponentInChildren<NotiBubble>();

        this._btnClick = this.GetComponent<Button>();

        this._tfPanel = this.transform;
        this._imgPanel = this.GetComponent<Image>();
        _initPosition = this.transform.localPosition;
    }
#endif
    public void ShowNoti(int noti)
    {
        if (_badge != null)
            _badge.SetText(noti);
    }
    public void ShowBubble(string txt)
    {
        if (_bubble != null)
            _bubble.ShowBubble(string.IsNullOrEmpty(txt)).SetText(txt);
    }
    public virtual void OnInit(System.Action<BaseButtonHandler> cbShow = null, System.Action<BaseButtonHandler> cbHide = null)
    {
        //_initPosition = this.transform.position;
        OnClickShow = cbShow;
        OnClickHide = cbHide;
    }
    public virtual void OnClick()
    {
        if (isShowing)
            OnHide();
        else
            OnShow();
    }
    public virtual void OnShow()
    {
        if (this.isShowing)
            return;

        this.isShowing = true;

        this._imgPanel.color = this._colorActive;
        this.transform.localPosition = new Vector3(this._initPosition.x, this._initPosition.y + 10);
        this.OnClickShow?.Invoke(this);
    }
    public virtual void OnHide()
    {
        if (!this.isShowing)
            return;

        this.isShowing = false;
        this._imgPanel.color = this._colorInActive;
        this.transform.localPosition = _initPosition;

        this.OnClickHide?.Invoke(this);
    }
}
