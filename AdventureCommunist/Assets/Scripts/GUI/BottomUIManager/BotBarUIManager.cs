using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBarUIManager : MonoBehaviour
{
    private static BotBarUIManager _instance;
    public static BotBarUIManager Instance
    {
        get
        {
            if(_instance == null)
                _instance = GameSceneManager.Instance._botBarUI;
            return _instance;
        }
    }

    public ComradeBotButtonHanler _comradeHandler;
    public CardInventoryBotButtonHanler _cardListHandler;
    public OperationBotButtonHanler _operationHandler;
    public EventBotButtonHanler _eventHandler;
    public StoreBotButtonHanler _storeHandler;

    [SerializeField]
    private List<BaseButtonHandler> allHandlers;

#if UNITY_EDITOR
    private void OnValidate()
    {
        allHandlers = new List<BaseButtonHandler>() { _comradeHandler, _cardListHandler , _operationHandler, _eventHandler, _storeHandler };
    }
#endif 
    
    public void OnInit()
    {
        foreach (BaseButtonHandler _handler in this.allHandlers)
            _handler.OnInit(cbShow: OnButtonClicked, cbHide: null);

    }
    public void OnButtonClicked(BaseButtonHandler _btn)
    {
        //call current handler to hide
        allHandlers.ForEach(x =>
        {
            if (x != _btn)
            {
                x.OnHide();
            }
        });
    }


}
