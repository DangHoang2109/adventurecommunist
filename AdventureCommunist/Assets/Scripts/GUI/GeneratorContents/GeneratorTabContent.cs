﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GeneratorTabContent : TabContent
{
    public BaseGenerator _prefabGenerator;
    public Transform _tfPanelContent;

    public List<BaseGenerator> allGenerators;


    private IndustryConfig _industryConfig;
    public IndustryConfig IndustryConfig => _industryConfig;

    public void ParseData(IndustryConfig _industryConfig)
    {
        this._industryConfig = _industryConfig;
        //get list of current active generator and next unlocking generator
        UserGameMapData MAP = IddleGameManager.Instance.CurrentMapData;
        //now we spawn all the damn shit generator

        int countLock = 0;
        //spawn them
        foreach(GeneratorConfig _genConfig in _industryConfig._generatorConfigs)
        {
            GeneratorData _data = MAP.GetData(_genConfig._typeID);
            if (_data == null)
            {
                countLock++;
            }

            if (countLock <= 1 )
            {
                SpawnAGenerator(_genConfig, _data);
            }

            if (_data == null && _genConfig._cost2Unlock.type != BoosterType.NONE && !string.IsNullOrEmpty(_genConfig._cost2Unlock.value))
            {
                IddleGameManager.Instance.CurrentBoosterData.AddCallbackBooster(_genConfig._cost2Unlock.type, OnChangeLockGeneratorGoal);
            }
        }
    }

    private void SpawnAGenerator(GeneratorConfig _genConfig, GeneratorData _data)
    {
        BaseGenerator _gen = Instantiate(_prefabGenerator, _tfPanelContent);
        _gen.ParseData(_genConfig, _data);

        allGenerators.Add(_gen);
    }

    public void OnChangeLockGeneratorGoal(BoosterCommodity _booster)
    {
        BaseGenerator lockingGen = this.allGenerators.Last();
        if(_booster.type == lockingGen.GeneratorConfig._cost2Unlock.type)
        {
            if(_booster.GetValueDouble() >= lockingGen.GeneratorConfig._cost2Unlock.GetValueDouble())
            {
                OnUnlockGenerator(lockingGen);
            }
        }
    }
    public void OnUnlockGenerator(BaseGenerator lockingGen)
    {
        lockingGen.UnlockMe();

        //spawn next generator
        GeneratorConfig _nxtConfig = IndustryConfig.NextOf(lockingGen.GeneratorConfig._typeID);
        SpawnAGenerator(_nxtConfig, null);

        IddleGameManager.Instance.CurrentBoosterData.RemoveCallbackBooster(lockingGen.GeneratorConfig._cost2Unlock.type, OnChangeLockGeneratorGoal);
        IddleGameManager.Instance.CurrentBoosterData.AddCallbackBooster(_nxtConfig._cost2Unlock.type, OnChangeLockGeneratorGoal);
    }

    public override void OnShow(int index, object data = null, UnityAction callback = null)
    {
        base.OnShow(index, data, callback);
    }
    public override void OnHide(int index, object data = null, UnityAction callback = null)
    {
        base.OnHide(index, data, callback);
    }
}
