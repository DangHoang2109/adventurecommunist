﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GeneratorTab : Tab
{
    public BoosterType _industryType;

    public Image imgIconActive;
    public TextMeshProUGUI tmpResourceWallet;

    public Image imgIconInactive;

    [Header("Science Badge")]
    public GameObject gBadge;
    public TextMeshProUGUI tmpAmountGenCollectScience;

    [Header("Transform")]
    public GameObject _gOn;
    public RectTransform _tfPanel;
    public float _activeWidth = 300f; //be change by tabbase depend on amount of tab
    public float _inactiveWidth = 105f;

    public void ParseIndustryData(IndustryConfig _industryConfig)
    {
        //get asset image
        this._industryType = _industryConfig._typeID;
        this.imgIconActive.sprite = this.imgIconInactive.sprite = _industryConfig._sprIcon;

        AssignCallBackToBooster();
    }

    private void AssignCallBackToBooster()
    {
        IddleGameManager.Instance.CurrentBoosterData.AddCallbackBooster(_industryType, OnChangeBoosterValue);
    }
    private void UnAssignCallBackToBooster()
    {
        IddleGameManager.Instance.CurrentBoosterData.RemoveCallbackBooster(_industryType, OnChangeBoosterValue);
    }
    private void OnChangeBoosterValue(BoosterCommodity _b)
    {
        if(_b.type == this._industryType)
        {
            tmpResourceWallet.SetText(_b.GetValueDouble().ToIddleUnitString());
        }
    }

    public override void OnChangeTab(bool isOn)
    {
        base.OnChangeTab(isOn);

        //bind hoặc unassign event get value khỏi manager
        this._tfPanel.sizeDelta = new Vector2(isOn ? _activeWidth : _inactiveWidth, this._tfPanel.sizeDelta.y);
        this._gOn.SetActive(isOn);
    }
}
