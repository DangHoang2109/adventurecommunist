﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratorTabBase : TabBase
{
    private EventDataConfig _eventConfig;

    [Header("UI")]
    public Transform _tfPanelTabs;
    public Transform _tfPanelTabContents;

    [Space(5f)]
    public GeneratorTab _prefabTab;
    public GeneratorTabContent _prefabTabContent;

    public List<BaseGenerator> AllGenerator;


    protected override void Start()
    {
        //base.Start();
    }
    public void SetupData()
    {
        AllGenerator = new List<BaseGenerator>();
        foreach(GeneratorTabContent _content in this.contents)
        {
            AllGenerator.AddRange(_content.allGenerators);
        }
    }
    public override void OnChangeTab()
    {
        base.OnChangeTab();
    }
    public void OnCompleteLoadData()
    {
        StartCoroutine(ieStartingParseTab());
    }
    private IEnumerator ieStartingParseTab()
    {
        YieldInstruction _waitFrame = new WaitForEndOfFrame();

        //get config about list generaotr will be used
        SetupConfigProp();

        //spawn the tab
        SetupTabUI();

        yield return _waitFrame;

        //spawn them
        OnSpawnItemInTabContent();

        //get data from the manager

        yield return _waitFrame;

        //parse it
        OnLoadDataToTabContent();

        yield return _waitFrame;

        SetupData();
    }

    private void SetupConfigProp()
    {
        this._eventConfig = IddleGameManager.Instance.CurrentEventConfig;

    }
    private void SetupTabUI()
    {
        ToggleGroup _tglGrp = _tfPanelTabs.GetComponent<ToggleGroup>();
        for (int i = 0; i < _eventConfig._industryConfigs.Count; i++)
        {
            Tab _tab = Instantiate(_prefabTab, _tfPanelTabs);
            TabContent _content = Instantiate(_prefabTabContent, _tfPanelTabContents);

            tabs.Add(_tab);
            contents.Add(_content);

            _tab.tabIndex = i;
            _content.tabIndex = i;
            _tab.group = _tglGrp;

            //Destroy(_tab.gameObject);
            //Destroy(_content.gameObject);
        }



        //if (_eventConfig._industryConfigs.Count >= this.tabs.Count)
        //    return;

        //for (int i = _eventConfig._industryConfigs.Count; i < tabs.Count; i++)
        //{
        //    Tab _tab = tabs[i];
        //    TabContent _content = contents[i];

        //    tabs.Remove(_tab);
        //    contents.Remove(_content);

        //    Destroy(_tab.gameObject);
        //    Destroy(_content.gameObject);
        //}


        this.Init();
    }
    private void OnSpawnItemInTabContent()
    {
        List<IndustryConfig> _industryConfigs = this._eventConfig._industryConfigs;

        for (int i = 0; i < _industryConfigs.Count; i++)
        {
            IndustryConfig _inndustry = _industryConfigs[i];
            (this.contents[i] as GeneratorTabContent).ParseData(_inndustry);
            (this.tabs[i] as GeneratorTab).ParseIndustryData(_inndustry);

        }
    }

    private void OnLoadDataToTabContent()
    {

    }
}
