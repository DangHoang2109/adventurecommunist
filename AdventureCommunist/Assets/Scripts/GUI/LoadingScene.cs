using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScene : BaseScene
{
    private float totalRes = 6f;
    private float currentRes = 0f;
    public override void OnParseData()
    {
        base.OnParseData();
        this.StartCoroutine(this.OnLoadData());

    }

    private void Update()
    {
        LoadingManager.callbackProgress.Invoke(currentRes / totalRes);
    }

    private IEnumerator OnLoadData()
    {
        yield return new WaitForEndOfFrame();
        yield return GameDataManager.Instance.OnLoadData();
        this.currentRes++;

        yield return new WaitForEndOfFrame();
        yield return TempDialogManager.Instance.PreloadDialog();
        this.currentRes++;
        yield return new WaitForEndOfFrame();

        GameManager.Instance.OnLoadScene(SceneName.MAIN_GAME);
        this.currentRes++;

        this.currentRes = totalRes;
        LoadingManager.callbackProgress.Invoke(1f);
        if (asyncScene != null)
        {
            asyncScene.allowSceneActivation = true;
        }
    }


    public AsyncOperation asyncScene;

    private IEnumerator LoadHomeScene()
    {
        asyncScene = SceneManager.LoadSceneAsync(SceneName.HOME);
        asyncScene.allowSceneActivation = false;
        this.currentRes++;
        while (!asyncScene.isDone)
        {
            float progress = asyncScene.progress;
            yield return null;
        }
        this.currentRes++;
        yield return asyncScene;
    }

    private IEnumerator LoadGameScene()
    {
        asyncScene = SceneManager.LoadSceneAsync(SceneName.GAME);
        asyncScene.allowSceneActivation = false;
        this.currentRes++;
        while (!asyncScene.isDone)
        {
            float progress = asyncScene.progress;
            yield return null;
        }
        this.currentRes++;
        yield return asyncScene;
    }


}
