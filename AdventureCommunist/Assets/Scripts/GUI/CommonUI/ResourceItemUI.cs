using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceItemUI : MonoBehaviour
{
    public Image _imgIcon;
    public TMPro.TextMeshProUGUI _tmpText;

    public BoosterCommodity _booster;
    public ResourceItemUI ParseBooster(BoosterCommodity booster)
    {
        this._booster = booster;
        GeneratorAssetConfig assets = GeneratorModelConfigs.Instance.GetAsset(booster.type);
        if(assets != null)
        {
            _imgIcon.sprite = assets._sprIcon;
            _tmpText.SetText(booster.ValueIddleUnit);

            if(!_tmpText.enableAutoSizing)
                _tmpText.rectTransform.sizeDelta = new Vector2(_tmpText.preferredWidth, _tmpText.preferredHeight);
        }
        return this;
    }
    public ResourceItemUI SetColorText(Color color)
    {
        _tmpText.color = color;

        return this;
    }
}
