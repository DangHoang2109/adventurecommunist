using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashBoosterUI : IdleBoosterUI
{
    public override void AssignCallback()
    {
        UserProfile.Instance.AddCallbackBooster(BoosterType.GOLD, this.OnChangeValueBooster);
    }
    public override void UnAssignCallback()
    {
        UserProfile.Instance.RemoveCallbackBooster(BoosterType.GOLD, this.OnChangeValueBooster);
    }
    public override void OnChangeValueBooster(BoosterCommodity booster)
    {
        base.OnChangeValueBooster(booster);
    }

    public void ClickBuyCash()
    {
        
    }
}
