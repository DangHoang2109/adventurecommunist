using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceNotiBubble : NotiBubble
{
    public ResourceItemUI _prefabResourceItem;
    public List<ResourceItemUI> readyResourceItems;

    public RectTransform _panelResource;

    public ResourceNotiBubble ParseData(List<BoosterCommodity> boosters)
    {
        if (readyResourceItems == null)
            readyResourceItems = new List<ResourceItemUI>();

        if(readyResourceItems.Count < boosters.Count)
        {
            int amount2Spawn = boosters.Count - readyResourceItems.Count;
            for (int i = 0; i < amount2Spawn; i++)
            {
                readyResourceItems.Add(Instantiate(_prefabResourceItem, _panelResource));
            }
        }

        for (int i = 0; i < readyResourceItems.Count; i++)
        {
            if (i >= boosters.Count)
                readyResourceItems[i].gameObject.SetActive(false);
            else
            {
                readyResourceItems[i].ParseBooster(boosters[i]);
            }
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_panelResource);
        return this;
    }

}
