using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NotiBubble : MonoBehaviour
{
    public Image _imgIcon;
    public TMPro.TextMeshProUGUI _tmpText;

    private float _totalTimeShow;
    private float _timeHasShowed;
    private bool _isShowTime;

    private bool _isShowing;

    protected void Update()
    {
        if(_isShowTime && _isShowing)
        {
            _timeHasShowed += Time.deltaTime;
            if(_timeHasShowed >= _totalTimeShow)
            {
                this.ShowBubble(false);
            }
        }
    }
    public virtual NotiBubble SetPermanentShow(bool isShowpermanent,float timeShow = 1f)
    {
        _isShowTime = !isShowpermanent;
        _totalTimeShow = timeShow;

        return this;
    }
    public virtual NotiBubble ShowBubble(bool isShow, float timeShow = -1)
    {
        _isShowing = isShow;
        this.gameObject.SetActive(isShow);

        _isShowTime = timeShow > 0;

        if (_isShowTime)
        {
            _timeHasShowed = 0;
            _totalTimeShow = timeShow;
        }

        return this;
    }
    public virtual T ShowBubble<T>(bool isShow, float timeShow = -1) where T : NotiBubble 
    {
        return ShowBubble(isShow, timeShow) as T;
    }
    public virtual NotiBubble HideIccon(bool isHide)
    {
        _imgIcon.gameObject.SetActive(isHide);
        return this;
    }
    public virtual NotiBubble SetSprite(Sprite spr)
    {
        _imgIcon.sprite = spr;
        return this;
    }
    public virtual NotiBubble SetText(string text)
    {
        ShowBubble(!string.IsNullOrEmpty(text));

        _tmpText.SetText(text);
        return this;
    }

    public virtual NotiBubble SetTextSize(float txtSize)
    {
        _tmpText.fontSize = txtSize;
        _tmpText.rectTransform.sizeDelta = new Vector2(_tmpText.preferredWidth, _tmpText.preferredHeight);
        return this;
    }
    public virtual NotiBubble SetTextColor(Color color)
    {
        _tmpText.color = color;
        return this;
    }
}
