using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class NotiBadge : MonoBehaviour
{
    public TextMeshProUGUI _tmpPro;
    
    public NotiBadge SetText(string txt)
    {
        if (string.IsNullOrEmpty(txt))
            this.gameObject.SetActive(false);

        this._tmpPro.SetText(txt);
        return this;
    }
    public NotiBadge SetText(int number)
    {
        if (number <= 0)
            this.gameObject.SetActive(false);

        this._tmpPro.SetText(number.ToString());
        return this;
    }
}
