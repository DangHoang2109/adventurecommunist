using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ButtonBulkBuy : MonoBehaviour
{
    public TextMeshProUGUI _tmpText;

    /// <summary>
    /// Assign this on editor
    /// </summary>
    public List<BulkBuyType> validOptions;

    [SerializeField] private int _currentOption = 0;
    public BulkBuyType CurrentOption => validOptions[_currentOption];

    public System.Action<BulkBuyType> _onChangeType;

#if UNITY_EDITOR
    private void OnValidate()
    {
        this._tmpText = GetComponentInChildren<TextMeshProUGUI>();
    }
#endif

    public void OnClickChangeTypeBuy()
    {
        if (++_currentOption >= validOptions.Count)
            _currentOption = 0;

        ChangeTextUI(GetStringBulkBuy(CurrentOption));

        _onChangeType?.Invoke(CurrentOption);
    }
    public void ChangeTextUI(string _txt)
    {
        _tmpText.SetText(_txt);
    }


    public static string GetStringBulkBuy(BulkBuyType _typeBulkBuy)
    {
        switch (_typeBulkBuy)
        {
            case BulkBuyType.ONE:
                return "x1";
            case BulkBuyType.TEN_PERCENT:
                return "10%";
            case BulkBuyType.HALF_PERCENT:
                return "50%";
            case BulkBuyType.MAX:
                return "MAX";
            default:
                return "";
        }
    }

}


public enum BulkBuyType
{
    ONE,
    TEN_PERCENT,
    HALF_PERCENT,
    MAX,
}