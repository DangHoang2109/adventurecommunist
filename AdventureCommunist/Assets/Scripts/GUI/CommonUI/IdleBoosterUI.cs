using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IdleBoosterUI : MonoBehaviour
{
    public Image imgIcon;
    public TextMeshProUGUI txtValue;

    public BoosterType _type;

    public virtual void AssignCallback()
    {
        IddleGameManager.Instance.CurrentBoosterData.AddCallbackBooster(this._type, this.OnChangeValueBooster);
    }
    public virtual void UnAssignCallback()
    {
        IddleGameManager.Instance.CurrentBoosterData.RemoveCallbackBooster(this._type, this.OnChangeValueBooster);
    }
    public virtual void ParseData(Sprite sprIcon)
    {
        this.imgIcon.sprite = sprIcon;
    }

    public virtual void OnChangeValueBooster(BoosterCommodity booster)
    {
        this.txtValue.text = booster.GetValueDouble().ToIddleUnitString();
    }
}
