﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BaseGenerator : IUpdateHandler
{
    private UserGameBoosterData _userGameBooster;
    public UserGameBoosterData UserGameBooster
    {
        get
        {
            if (_userGameBooster == null)
                _userGameBooster = IddleGameManager.Instance.CurrentBoosterData;

            return _userGameBooster;
        }
    }

    public GeneratorBulkBuyCalculator _bulkBuyCalculator;

    #region Data Prop
    protected GeneratorData _data;
    public GeneratorData Data => _data;

    protected bool _isAuto;

    protected double _boostTimeMultiplier = 0;

    protected GeneratorConfig _generatorConfig;
    public GeneratorConfig GeneratorConfig => _generatorConfig;

    public double AmountGenerator
    {
        get => this.Data._amountGenerator;
        set
        {
            this.Data._amountGenerator = value;


            UpdateUITimer();
        }
    }
    public double TimeLeftToProfit
    {
        get => this.Data._timeLeftToProfit;
        set => this.Data._timeLeftToProfit = value;
    }
    #endregion Config

    #region UI
    [Header("UI")]
    public GeneratorUI _GeneratorUI;

    protected void UpdateUITimer()
    {
        if(Data._amountGenerator >= Data._nextScienceObjective)
        {
            double scienceCollected = Data.AchiveScienceObjectiveGoal();
            //show the science bubble
            _GeneratorUI.ShowScienceBubble(scienceCollected);
        }
        _GeneratorUI.UpdateUITimerAmount(Data._amountGenerator, Data._nextScienceObjective);


        _GeneratorUI.UpdateUITimerProfit(TotalTimeToProfit < 1 ? $"{ValueProfitPerSec.ToIddleUnitString()}/SEC" : ProfitPerInterval.GetIddleUnitString);
    }
    #endregion UI

    public double TotalTimeToProfit
    {
        get
        {

            if (_generatorConfig == null)
                return 0;
            if (_boostTimeMultiplier > 0)
                return _generatorConfig._intervalProfit / _boostTimeMultiplier;


            return _generatorConfig._intervalProfit;
        }
    }

    private double _valueProfitPerInterval;
    private BoosterCommodity _profit;
    public BoosterCommodity ProfitPerInterval
    {
        get
        {
            return _profit.Set(_valueProfitPerInterval * this.AmountGenerator);
        }
    }
    public double ValueProfitPerSec
    {
        get
        {
            return _valueProfitPerInterval * AmountGenerator / _generatorConfig._intervalProfit;
        }
    }
    public BoosterCommodity AutoProfitPerSec
    {
        get
        {
            if(_isAuto)
                return new BoosterCommodity(_profit.type, ValueProfitPerSec);
            return new BoosterCommodity(_profit.type, 0);
        }
    }

    public List<BoosterCommodity> CostToBuyOne
    {
        get
        {
            return this.GeneratorConfig._cost2BuyOne;
        }
    }

    public bool IsUnlocked => AmountGenerator > 0;
    private void OnDestroy()
    {
        UnAssignCallbackManager();
    }
    protected virtual void AssignCallbackManager()
    {
        AssignToUpdateManager();
        AssignToBooster();

        if (_bulkBuyCalculator == null)
            _bulkBuyCalculator = new GeneratorBulkBuyCalculator();
        _bulkBuyCalculator.SetUp(this);
    }
    protected virtual void UnAssignCallbackManager()
    {
        UnAssignToUpdateManager();
        UnAssignToBooster();
    }
    protected void AssignToBooster()
    {
        UserGameBooster.AddCallbackBooster(this.GeneratorConfig._typeID, OnChangeThisAmountBooster);
    }
    protected void UnAssignToBooster()
    {
        UserGameBooster.RemoveCallbackBooster(this.GeneratorConfig._typeID, OnChangeThisAmountBooster);
    }

    //Bản thân amount của cái này bị thay đổi
    protected void OnChangeThisAmountBooster(BoosterCommodity _booster)
    {
        if(_booster.type == this.GeneratorConfig._typeID)
            this.AmountGenerator = _booster.GetValueDouble();
    }

    public void SetAutoState(bool isAuto)
    {
        this._isAuto = isAuto;
    }
    public void ParseData(GeneratorConfig _genConfig, GeneratorData _genData)
    {
        if (_genData == null)
            this._data = new GeneratorData(_genConfig._typeID);
        else
            this._data = _genData;

        _data.BindConfig(_genConfig);

        this._generatorConfig = _genConfig;

        _valueProfitPerInterval = _genConfig._profit.GetValueDouble();

        this._profit = new BoosterCommodity(_genConfig._profit.type, _valueProfitPerInterval);

        if(IsUnlocked)
            AssignCallbackManager();

        ParseDataUI();
    }
    public void UnlockMe()
    {

        UserGameBooster.AddValueBooster(this.GeneratorConfig._typeID, 1);
        SetAmountGenerator(1);

        _GeneratorUI.SetUnlock(IsUnlocked, this._generatorConfig);
        AssignCallbackManager();

        ChangeUI();
    }

    public void SetAmountGenerator(double amount)
    {
        this.AmountGenerator = amount;
    }
    public void AddAmountGenerator(double amount)
    {
        this.AmountGenerator += amount;
    }

    protected virtual void ParseDataUI()
    {
        _GeneratorUI._imgIcon.sprite = this._generatorConfig._sprIcon;

        Sprite _sprProfit = GeneratorModelConfigs.Instance.GetAsset(IddleGameManager.Instance.CurrentSceneHandler._id, GeneratorConfig._profit.type)._sprIcon;
        _GeneratorUI._timerProduceProfit.SetExtraIcon(_sprProfit);

        _GeneratorUI.SetUnlock(IsUnlocked, this._generatorConfig);
        _GeneratorUI._buttonBuy.Bind(this);
        ChangeUI();
    }
    public virtual void ChangeUI()
    {
        this._GeneratorUI._timerProduceProfit.SetTimer(TimeLeftToProfit, TotalTimeToProfit, ProfitPerInterval.GetValueDouble().ToIddleUnitString()); // "Profit/SEC"
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        OnCustomUpdate(Time.deltaTime);
    }

    protected virtual void OnCustomUpdate(float deltaTime)
    {
        TimeLeftToProfit += deltaTime;
        if(TimeLeftToProfit >= TotalTimeToProfit)
        {
            TimeLeftToProfit = 0;
            if (AmountGenerator > 0)
                OnCreateProfit();
        }

        ChangeUI();
    }   
    protected virtual void OnCreateProfit()
    {
        UserGameBooster.AddValueBooster(this.ProfitPerInterval);
    }
    public void OnClickBuy(double amountCanBuy)
    {
        //sub booster
        foreach(BoosterCommodity b in this.CostToBuyOne)
        {
            double cost = b.GetValueDouble() * amountCanBuy;
            if (this.UserGameBooster.IsHasBooster(b.type, cost))
                UserGameBooster.UseBooster(b.type, cost);
        }

        //add booster of this
        UserGameBooster.AddValueBooster(this.GeneratorConfig._typeID, amountCanBuy);
    }

    public void SkipTime(double timeSkip)
    {
        Debug.LogError("TEMP FUNCTION, NOT CORRENT WITH TAYLOR SERIES OF EXPONENT MATH");
        //create profit by the time skipped;
        if (this._isAuto)
        {
            UserGameBooster.AddValueBooster(this.GeneratorConfig._profit.type, this.ProfitPerInterval.Value * (timeSkip/TotalTimeToProfit));
        }
        else
        {
            if (timeSkip + TimeLeftToProfit >= TotalTimeToProfit)
                UserGameBooster.AddValueBooster(this.ProfitPerInterval);
        }

        //adjust the timer
        this.TimeLeftToProfit += timeSkip;
        if (TimeLeftToProfit >= TotalTimeToProfit)
            TimeLeftToProfit -= TotalTimeToProfit;
    }
}

public class GeneratorBulkBuyCalculator
{
    public BulkBuyType _type = BulkBuyType.MAX;
    public BaseGenerator _host;

    private ButtonBulkBuy _bulkBuyButton;
    public ButtonBulkBuy BulkBuyButton
    {
        get
        {
            if (_bulkBuyButton == null)
                _bulkBuyButton = GameSceneManager.Instance._buttonBulkBuy;
            return _bulkBuyButton;
        }
    }
    private void OnChangeBulkBuy(BulkBuyType _t)
    {
        this._type = _t;
        OnChangeAmountBulkBuy(null);
    }

    public void SetUp(BaseGenerator _host)
    {
        this._host = _host;
        List<BoosterCommodity> cost2Buy = _host.CostToBuyOne;
        foreach (BoosterCommodity b in cost2Buy)
        {
            _host.UserGameBooster.AddCallbackBooster(b.type, this.OnChangeAmountBulkBuy);
        }

        BulkBuyButton._onChangeType += this.OnChangeBulkBuy;
    }

    private void OnChangeAmountBulkBuy(BoosterCommodity b)
    {
        this._host._GeneratorUI._buttonBuy.SetAmountCanBuy(OnCalculateAmountBuy());
    }
    public List<BoosterCommodity> SearchMissingResource(ref List<BoosterCommodity> readyList)
    {
        if (readyList == null)
            readyList = new List<BoosterCommodity>();
        else
            readyList.Clear();

        List<BoosterCommodity> cost2Buy = _host.CostToBuyOne;
        foreach (BoosterCommodity b in cost2Buy)
        {
            if (!_host.UserGameBooster.IsHasBooster(b.type, b.GetValueDouble()))
                readyList.Add(b);
        }


        return readyList;
    }

    private double OnCalculateAmountBuy()
    {
        //check max buy
        double amountMaxCanBuy = double.MaxValue ;
        List<BoosterCommodity> cost2Buy = _host.CostToBuyOne;
        foreach(BoosterCommodity b in cost2Buy)
        {
                amountMaxCanBuy = System.Math.Min(amountMaxCanBuy, _host.UserGameBooster.MaxBoosterDiv(b.type, b.GetValueDouble()));
        }

        if (this._type == BulkBuyType.ONE)
            return System.Math.Min(amountMaxCanBuy, 1);

        double percentToBuy = 1;
        switch (this._type)
        {
            case BulkBuyType.HALF_PERCENT:
                percentToBuy = 0.5;
                break;
            case BulkBuyType.TEN_PERCENT:
                percentToBuy = 0.1;
                break;
        }

        return GameUtils.RoundDownValue(amountMaxCanBuy * percentToBuy);
    }
} 


[System.Serializable]
public class GeneratorUI
{
    public Image _imgIcon;

    /// <summary>
    /// timer for produce profit
    /// </summary>
    public ITimerUI _timerProduceProfit;

    public IButtonBuyGenerator _buttonBuy;

    /// <summary>
    /// timer for produce profit
    /// </summary>
    public ITimerUI _timerAmount;

    public GeneratorUILock _uiLock;

    public GameObject _gScienceCollectable;
    public ResourceNotiBubble _scienceBubble;

    public void SetUnlock(bool isUnlocked, GeneratorConfig _genConfig)
    {
        _uiLock._gLock.SetActive(!isUnlocked);
        if (!isUnlocked)
        {
            _uiLock.Parse(_genConfig._industryName, _genConfig._cost2Unlock);
        }

        _timerAmount.gameObject.SetActive(isUnlocked);
        _gScienceCollectable.SetActive(isUnlocked);
        _buttonBuy.gameObject.SetActive(isUnlocked);
        _timerProduceProfit.gameObject.SetActive(isUnlocked);
    }
    public void UpdateUITimerAmount(double amountGenerator, double nextGoal)
    {
        this._timerAmount.SetTimer(amountGenerator, nextGoal, amountGenerator.ToIddleUnitString());
    }
    public void UpdateUITimerProfit(string text)
    {
        this._timerProduceProfit.SetExtraText(text);
    }
    public void ShowScienceBubble(double amountSciene)
    {
        _gScienceCollectable.gameObject.SetActive(amountSciene > 0);
        _scienceBubble
            .ShowBubble<ResourceNotiBubble>(amountSciene > 0)
            .ParseData(new List<BoosterCommodity>() { new BoosterCommodity(BoosterType.SCIENCE, amountSciene) })
            .SetPermanentShow(true);
    }
}
[System.Serializable]
public class GeneratorUILock
{
    public GameObject _gLock;
    public TMPro.TextMeshProUGUI _tmpUnlockTitle, _tmpUnlockGoal;
    public Image _imgSpriteUnlockGoal;

    public void Parse(string genName, BoosterCommodity _goal)
    {
        string genNameFormetd = genName.ToUpper();
        this._tmpUnlockTitle.SetText($"UNLOCK {genNameFormetd}");
        this._tmpUnlockGoal.SetText($"SAVE {_goal.GetValueDouble().ToIddleUnitString()} {genNameFormetd}S");

        _imgSpriteUnlockGoal.sprite = GeneratorModelConfigs.Instance.GetAsset(_goal.type)?._sprIcon;
    }
}