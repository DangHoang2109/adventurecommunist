﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
public class ITimerUI : MonoBehaviour
{
    public Image _imgFill;
    public TextMeshProUGUI _tmpCurrentValue;

    public Image _imgExtraIcon;
    public TextMeshProUGUI _tmpExtraText;

    public GameObject gStaticBar;

    public const float FRAME_LENGTH = 0.02f;
    public ITimerUI SetFill(float fill, float timeAnimation = -1)
    {
        if(fill != this._imgFill.fillAmount)
        {
            if (timeAnimation <= 0)
                this._imgFill.fillAmount = fill;
            else
                this._imgFill.DOFillAmount(fill, timeAnimation);
        }
        return this;
    }
    public ITimerUI SetValue(double currentValue)
    {
        this._tmpCurrentValue.SetText(currentValue.ToIddleUnitString());
        return this;
    }
    public void SetTimer(double _curValue, double _totalValue, string extraText = "")
    {
        SetExtraText(extraText);

        if (_totalValue < FRAME_LENGTH)
        {
            if (!this.gStaticBar.activeInHierarchy)
                gStaticBar.gameObject.SetActive(true);

            return;
        }

        SetFill((float)(_curValue / _totalValue))
            .SetValue(_curValue);


    }
    public void SetExtraText(string extraText)
    {
        if (!string.IsNullOrEmpty(extraText) && _tmpExtraText != null)
        {
            _tmpExtraText.SetText(extraText);
            if (!_tmpExtraText.enableAutoSizing)
                _tmpExtraText.rectTransform.sizeDelta = new Vector2(_tmpExtraText.preferredWidth, _tmpExtraText.rectTransform.sizeDelta.y);
        }
    }
    public void SetExtraIcon(Sprite _spr)
    {
        if(_imgExtraIcon != null)
        _imgExtraIcon.sprite = _spr;
    }
}
