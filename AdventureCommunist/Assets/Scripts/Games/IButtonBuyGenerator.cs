using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
public class IButtonBuyGenerator : MonoBehaviour, IPointerClickHandler
{
    public TextMeshProUGUI _tmpAmountBuy;
    public Button _btnBuy;
    public ResourceNotiBubble _notiBubble;

    public string _genNameFormated;
    public BaseGenerator _host;
    public double amountCanBuy;


    private List<BoosterCommodity> resourceCostState;

    private void OnValidate()
    {
        this._btnBuy = this.GetComponent<Button>();
        this._tmpAmountBuy = this.GetComponentInChildren<TextMeshProUGUI>();

    }

    public void Bind(BaseGenerator _gen)
    {
        this._host = _gen;
        this._genNameFormated = _gen.GeneratorConfig._industryName.ToUpper();

    }
    public void SetInterractable(bool inter)
    {
        this._btnBuy.interactable = inter;
    }

    public void OnClickButton()
    {
        //buy if can
        this._host.OnClickBuy(this.amountCanBuy);
    }

    public void SetAmountCanBuy(double amountBuy)
    {
        amountCanBuy = amountBuy;
        SetInterractable(amountBuy > 0);

        string amoutnUI = amountBuy == 0 ? "1" : amountBuy.ToIddleUnitString();
        this._tmpAmountBuy.SetText($"BUY x{amoutnUI} {_genNameFormated}");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //show cost bubble
        _host._bulkBuyCalculator.SearchMissingResource(ref this.resourceCostState);
        //parse this to bubble
        _notiBubble.ShowBubble<ResourceNotiBubble>(true, 1f).ParseData(_host.CostToBuyOne);
        _notiBubble.readyResourceItems.ForEach(x => x.SetColorText(this.resourceCostState.Find(b => b.type == x._booster.type) != null ? Color.red : Color.green));

    }
}
