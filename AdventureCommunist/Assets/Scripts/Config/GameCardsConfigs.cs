﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


/// <summary>
/// 
/// </summary>
public enum CardTierID
{
    NONE = -1,
    COMMON = 0,
    RARE,
    EPIC, //card tím
    SUPREME //card đỏ
}

/// <summary>
/// enum ID các loại hình boosting và perk trong game
/// </summary>
public enum BoostingTypeID
{
    NONE = -1,

    MINESHAFT_PROFIT = 0, //tăng profit của mine                            //default: x1
    MINESHAFT_SPEED = 1, //tăng tốc độ sinh profit của mine                 //default: +0%
    
    ROCK_PROFIT_COIN, //TĂNG % PROFIT COIN DROP KHI DESTROY ROCK            //default: +0%
    DELIVERY_PROFIT_COIN, //TĂNG % PROFIT COIN DROP TRONG DELIVERY BARREL   //default: +0%
    
    GOBLIN_SPAWN_TIME, //GIẢM THỜI GIAN CẦN ĐỂ FREE SPAWN 1 GOB             // default: -0
    GOBLIN_LIMIT, //TĂNG LIMIT GOBLIN CÓ TRÊN SÂN                           // default: +0
    GOBLIN_LEVEL, //TĂNG LEVEL CỦA GOBLIN MUA HIỆN TẠI LÊN => LV CỦA GOB FREE VÀ GOB ADS CŨNG TĂNG THEO //default: +0
    GOBLIN_DISCOUNT, //TĂNG DISCOUNT KHI MUA GOBLIN                         // default: x1 => :1
    GOBLIN_DAMAGE_CRIT, //TĂNG X CỦA CRIT HIT CỦA GOBLIN                    // default x1
    GOBLIN_CRIT_CHANCE, //TĂNG XÁC SUẤT RA CRIT HIT CỦA GOBLIN              // default: +0%

    CHEST_CARDS_AMOUNT, //TĂNG LƯỢNG CARD GIFT TỪ CHEST LÊN                 //default +0%
    EVENT_REWARD_AMOUNT, //TĂNG LƯỢNG REWARD NHẬN ĐƯỢC TỪ EVENT LÊN         //default +0%
}


public enum BoostingTargetType
{
    NONE = -1,

    ROCKS,
    GOBLINS,
    MINESHAFTS,
    CHECKPOINTS,
    DELIVERIES,
    CHESTS,
    EVENTS
}

[System.Serializable]
/// <summary>
/// Giá trị của Boosting Type này khi CHƯA CÓ AI ĐƯA VÀO
/// CÓ THỂ COI LÀ GIÁ TRỊ BOOSTING KHI KO CÓ CARD BOOSTER NÀO CẢ
/// </summary>
public class BoostingInitialConfig
{
    public BoostingTypeID _id;
    public float _boostingValue;
    public BoostingFormular _formular;
    public string _formatFormular;

    public BoostingInitialConfig()
    {

    }
    public BoostingInitialConfig(BoostingInitialConfig c)
    {
        _id = c._id;
        _boostingValue = c._boostingValue;
        _formatFormular = c._formatFormular;
        _formular = c._formular;
    }
}

/// <summary>
/// Lưu config các card 
/// </summary>
[CreateAssetMenu(menuName = "Configs/Game/GameCardsConfigs", fileName = "GameCardsConfigs")]
public class GameCardsConfigs : ScriptableObject
{
    public static GameCardsConfigs Instance
    {
        get
        {
            return LoaderUtility.Instance.GetAsset<GameCardsConfigs>("Game/Config/GameCardsConfigs");
        }
    }

    public List<CardConfig> CardConfigs;

    public CardConfig GetCardConfig(int _cardId)
    {
        return CardConfigs.Find(x => x.cardId == _cardId);
    }

    [ContextMenu("set config")]
    private void EditorSetConfig()
    {
        foreach(CardConfig c in this.CardConfigs)
        {
            c._formularBoosting = BoostingFormular.TIMES;
            c._targetType = BoostingTargetType.MINESHAFTS;
        }
        //for (int i = 1; i <= 14; i++)
        //{
        //    MineID mineID = (MineID)i;
        //    CardConfigs.Add(new CardConfig()
        //    {
        //        cardId = 100 + i,
        //        _boostTypeID = BoostingTypeID.MINESHAFT_PROFIT,
        //        _boostingMine = mineID,
        //        name = $"Manager - {mineID.ToString().ToLowerInvariant()}",
        //        description = $"Automate and increase the profit of the {mineID.ToString().ToLowerInvariant()} mineshaft",
        //        formatBoosting = "x{0}"
        //    });
        //}
        //for (int i = 1; i <= 14; i++)
        //{
        //    MineID mineID = (MineID)i;
        //    CardConfigs.Add(new CardConfig()
        //    {
        //        cardId = 200 + i,
        //        _boostTypeID = BoostingTypeID.MINESHAFT_PROFIT,
        //        _boostingMine = mineID,
        //        name = $"Manager - {mineID.ToString().ToLowerInvariant()}",
        //        description = $"Automate and increase the profit of the {mineID.ToString().ToLowerInvariant()} mineshaft",
        //        formatBoosting = "x{0}"
        //    });
        //}
    }
    
}

public static class GameCardID
{
    public const int MINE_FORGED = 100;
    public const int MINE_AMETHYST = 101;
    public const int MINE_CITRINE = 102;
    public const int MINE_AGATE = 103;
    public const int MINE_TOPAZ = 104;
    public const int MINE_OPAL = 105;
    public const int MINE_JADE = 106;
    public const int MINE_ONYX = 107;
    public const int MINE_SAPPHIRE = 108;
    public const int MINE_TOURMALINE = 109;
    public const int MINE_AQUAMARINE = 110;
    public const int MINE_EMERALD = 111;
    public const int MINE_DIAMOND = 112;
    public const int MINE_GARNET = 113;
    public const int MINE_HELIODOR = 114;

    public const int MANAGER_AMETHYST = 201;
    public const int MANAGER_CITRINE = 202;
    public const int MANAGER_AGATE = 203;
    public const int MANAGER_TOPAZ = 204;
    public const int MANAGER_OPAL = 205;
    public const int MANAGER_JADE = 206;
    public const int MANAGER_ONYX = 207;
    public const int MANAGER_SAPPHIRE = 208;
    public const int MANAGER_TOURMALINE = 209;
    public const int MANAGER_AQUAMARINE = 210;
    public const int MANAGER_EMERALD = 211;
    public const int MANAGER_DIAMOND = 212;
    public const int MANAGER_GARNET = 213;
    public const int MANAGER_HELIODOR = 214;

    public const int ROCK_PROFIT = 300; //x
    public const int SPAWN_TIME = 301; //x
    public const int MINE_SPEED = 302;  //x
    public const int GOBLIN_DISCOUNT = 303; //x
    public const int CRIT_CHANCE = 304;
    public const int DELIVERY_PROFIT = 305; //x
    public const int SPAWN_TIME_CHECKPOINT = 306; //x
    public const int CARDS_IN_CHEST = 307; 
    public const int EVENT_REWARD = 308;

    public const int MINE_PROFIT = 400;  //x
    public const int GOBLIN_LIMIT = 401;  //x
    public const int MINE_PROFIT_CHECKPOINT = 402; //x
    public const int GOBLIN_LEVEL = 403; //x
    public const int CRIT_DAMAGE = 404;
}

/// <summary>
/// Config về một card A
/// </summary>
[System.Serializable]
public class CardConfig
{
    public int cardId;
    public BoostingTypeID _boostTypeID;

    //Có giá trị nếu card này boost cho một industry hoặc generator cụ thể
    //Không có giá trị nếu card này boost cho toàn thể
    //Nếu giá trị none thì dùng name của BoostingType Asset
    public BoosterType _boostingResource;

    /// <summary>
    /// Loại target show trên card
    /// </summary>
    public BoostingTargetType _targetType;

    /// <summary>
    /// Chỉ số multiplier của card này là kiểu cộng trừ nhân chia?
    /// </summary>
    public BoostingFormular _formularBoosting;

    //TODO: put UI Config, để thêm nếu thích và cần
    public string name;
    public string description;
    public Sprite sprIcon;
    public string formatBoosting;

    public string BoostTypeToTitle
    {
        get
        {
            switch (this._boostTypeID)
            {
                case BoostingTypeID.NONE:
                    return "";
                case BoostingTypeID.MINESHAFT_PROFIT:
                case BoostingTypeID.ROCK_PROFIT_COIN:
                case BoostingTypeID.DELIVERY_PROFIT_COIN:

                    return "PROFIT";

                case BoostingTypeID.MINESHAFT_SPEED:
                    return "SPEED";

                case BoostingTypeID.GOBLIN_SPAWN_TIME:
                    return "SPAWN TIME";
                case BoostingTypeID.GOBLIN_LIMIT:
                    return "LIMIT+";
                case BoostingTypeID.GOBLIN_LEVEL:
                    return "LEVEL+";
                case BoostingTypeID.GOBLIN_DISCOUNT:
                    return "DISCOUNT";
                case BoostingTypeID.GOBLIN_DAMAGE_CRIT:
                    return "CRIT DAMAGE";
                case BoostingTypeID.GOBLIN_CRIT_CHANCE:
                    return "CRIT CHANCE";
                case BoostingTypeID.CHEST_CARDS_AMOUNT:
                    return "CARD+";
                case BoostingTypeID.EVENT_REWARD_AMOUNT:
                    return "REWARDS+";
                default:
                    return "";
            }
        }
    }
}

/// <summary>
/// Config về cách mà Card A được dùng trong event X như thế nào
/// Điển hình cùng là card CritChance, tại main game tier nó là legend, tại event khác nó là rare
/// Điển hình hệ số giảm thời gian của card SpawnTime ở main game là 1m30s mỗi level, ở event là 5m mỗi level
/// </summary>
[System.Serializable]
public class EventCardUsageConfig
{
    /// <summary>
    /// ID của card ref đến
    /// </summary>
    public int idCard;

    public CardTierID tier;

    [System.NonSerialized]
    private CardConfig _cardConfig = null;
    public CardConfig CardConfig
    {
        get
        {
            if (_cardConfig == null)
                _cardConfig = GameCardsConfigs.Instance.GetCardConfig(this.idCard);
            return _cardConfig;
        }
    }

    ///// <summary>
    ///// Giá trị boost nhận được
    ///// </summary>
    //public IddleStructExponentPair boostUpgradeMultiplier;
    public List<double> boostUpgradeMultipliers;

    /// <summary>
    /// Cột mốc bắt đầu unlock
    /// Tại maingame là index các map
    /// Tại event thì config ẩn là index các gate/mine destroy
    /// </summary>
    public int indexMineUnlocking;

    /// <summary>
    /// Max level của card này có thể upgrade đến
    /// </summary>
    public int maxLevel;

    public double GetBoostingValueByLevel(int level)
    {
        int capLevel = level - 1;

        if (capLevel >= 0 && capLevel < this.boostUpgradeMultipliers.Count)
            return boostUpgradeMultipliers[capLevel];

        return GameEventDataConfigs.Instance.GetInitialBoostingValue(this.CardConfig._boostTypeID);
        //return boostUpgradeMultiplier.initialValue * Mathf.Pow(boostUpgradeMultiplier.multiplierEachStep, level - 1);
    }

}