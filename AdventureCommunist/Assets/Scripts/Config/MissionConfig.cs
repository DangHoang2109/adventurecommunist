﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MissionConfig 
{
    //public string _missionName;
    public EventID _eventID;
    public MissionRequirementType _requireType;

    //None nếu require ko phải là resource requirement
    //public bool _isIndustryMission;
    public BoosterType _requireResource;
    public double _requireAmount;

    /// <summary>
    /// Instead of using ChestBoosterCommudity, we use this
    /// As rank 1 we not introduce chest yet
    /// </summary>
    public BoosterCommodity _rewardComplete;
    public ChestID _rewardChestID;

    private Sprite _icon;
    public Sprite Icon
    {
        get
        {
            switch (this._requireType)
            {
                case MissionRequirementType.NONE:
                    return null;
                case MissionRequirementType.UNLOCK_INDUSTRY:
                case MissionRequirementType.OWN_RESOURCE:
                case MissionRequirementType.COLLECT_RESOURCE:
                    GeneratorConfig generator = GameEventDataConfigs.Instance.GetConfig(this._eventID).GetGenerator(this._requireResource);

                    return generator._sprIcon;

                case MissionRequirementType.SPEND_SCIENCE:
                case MissionRequirementType.UPGRADE_CARDS:
                case MissionRequirementType.TRADE_COMRADE:
                    return MissionConfigAssets.Instance.GetSprite(this._requireType);
            }

            //Load icon dynamic from GameAssetConfig
            //with require of resourrce, return resource icon
            //with another, return config icon
            return _icon;
        }
    }

    public string RequirementString
    {
        get
        {
            string format = MissionConfigAssets.Instance.GetRequirementFormat(this._requireType);

            switch (this._requireType)
            {
                default:
                case MissionRequirementType.NONE:
                    return "";
                case MissionRequirementType.UNLOCK_INDUSTRY:
                case MissionRequirementType.OWN_RESOURCE:
                case MissionRequirementType.COLLECT_RESOURCE:
                    GeneratorConfig generator = GameEventDataConfigs.Instance.GetConfig(this._eventID).GetGenerator(this._requireResource);

                    return string.Format(format, generator._industryName);

                case MissionRequirementType.SPEND_SCIENCE:
                case MissionRequirementType.UPGRADE_CARDS:
                case MissionRequirementType.TRADE_COMRADE:
                    return format;
            }

        }
    }

    public string MissionName
    {
        get
        {
            return MissionConfigAssets.Instance.GetMissionName(this._requireType, this._requireResource);
        }
    }
}

public enum MissionRequirementType
{
    NONE = -1,
    UNLOCK_INDUSTRY = 0,
    OWN_RESOURCE, //chỉ cần wallet hiện tại có đủ lượng này là đạt
    COLLECT_RESOURCE,//bắt đầu tính lượng collect từ lúc mission được unlock

    SPEND_SCIENCE,
    UPGRADE_CARDS,
    TRADE_COMRADE,

    //group to collect resource type
    //COLLECT_CARDS,
    //COLLECT_SCIENCE,
}