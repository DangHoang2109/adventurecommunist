﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum EventID
{
    NONE = -1,
    MOTHERLAND = 0,

    SPACE_FORCE,
    COMMUNISTCRUSADE,
    ANEW_ATLANTIS,
    COMRADE_COWBOYS,
    NINJA_UNION,
    WINTER_MOTHERLAND,
    STONE_STATE,
    ZOMBIE_REVOLUTION,
    SHIELD_UP,
    QUEST_4_OIL,
    POTATO_EXPORT,
    POWER_UNDERWHELMING,
}

[CreateAssetMenu(menuName = "Configs/Game/EventConfigs", fileName = "GameEventDataConfigs")]
public class GameEventDataConfigs : ScriptableObject
{
    public static GameEventDataConfigs Instance
    {
        get
        {
            return LoaderUtility.Instance.GetAsset<GameEventDataConfigs>("Game/Config/GameEventDataConfigs");
        }
    }

    public EventDataConfig[] ConfigForEachEvent;

    [Space(10)]
    [Tooltip("Giá trị ban đầu của các boosting perk")]
    public List<BoostingInitialConfig> InitialBoostingValue;


    public float GetInitialBoostingValue(BoostingTypeID _id)
    {
        BoostingInitialConfig _config = InitialBoostingValue.Find(x => x._id == _id);
        return _config == null ? 0 : _config._boostingValue;
    }



    public EventDataConfig GetConfig(EventID _eventID)
    {
        return ConfigForEachEvent.ToList().Find(x => x._eventID == _eventID);
    }
    private void OnValidate()
    {
        foreach(EventDataConfig _event in ConfigForEachEvent)
        {
            for (int i = 0; i < _event._industryConfigs.Count; i++)
            {
                IndustryConfig _industry = _event._industryConfigs[i];
                _industry.indexIndustry = i;
                _industry._typeID = (BoosterType)((i + 1) * 100);

                _industry._cost2Unlock.type = (i == 0 ? BoosterType.NONE : (BoosterType)((i )*100));

                for (int j = 0; j < _industry._generatorConfigs.Count; j++)
                {
                    GeneratorConfig _gen = _industry._generatorConfigs[j];
                    _gen._typeID = (BoosterType)((int)_industry._typeID + (j + 1));
                    _gen.indexIndustry = i;

                    _gen._cost2Unlock.type = (j == 0 ? ( i == 0 ? BoosterType.NONE : _industry._cost2Unlock.type ): (BoosterType)((int)_gen._typeID -1));

                    if(j == 0)
                    {
                        _gen._cost2Unlock.value = _industry._cost2Unlock.value;
                    }

                    _gen._profit.type = (j == 0 ? _industry._typeID : (BoosterType)((int)_gen._typeID - 1));

                    if (j > 0)
                    {
                        _gen._intervalProfit = _industry._generatorConfigs[j - 1]._intervalProfit * 2;
                        _gen._profit.value = (_industry._generatorConfigs[j - 1]._profit.GetValue() + 1).ToString();
                    }

                    if (_gen._cost2BuyOne == null || _gen._cost2BuyOne.Count == 0)
                    {
                        _gen._cost2BuyOne = new List<BoosterCommodity>()
                        {
                            new BoosterCommodity(){ type = _gen._cost2Unlock.type},
                            new BoosterCommodity(){ type = _industry._typeID},
                            new BoosterCommodity(){ type = BoosterType.COMRADE, value = (i+1).ToString()}
                        };
                    }

                    if(_gen._cost2BuyOne.Count == 3)
                    {
                        _gen._cost2BuyOne[0].type = (j == 0 ? BoosterType.NONE : (BoosterType)((int)_gen._typeID - 1));
                        _gen._cost2BuyOne[1].type = _industry._typeID;

                    }

                    _gen._cost2BuyOne.Last().value = (i + 1).ToString();

                    _gen.sciencePerObjecttive = i + 1;

                }
            }
        }
    }

    #region Editor
    [ContextMenu("InputMissionAsset")]
    private void Editor_InputMissionAsset()
    {
        List<MissionConfigAsset> configs = new List<MissionConfigAsset>()
        {
            new MissionConfigAsset()
            {
                 _name = "Break Ground!",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.UNLOCK_INDUSTRY,
                  _requirementStringFormat = "Unlock {0}",
                  _requirementResource = BoosterType.I5,
            },

            
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I1T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I2T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I3T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I4T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.OWN_RESOURCE,
                  _requirementStringFormat = "Own {0}",
                  _requirementResource = BoosterType.I5T12,
            },

            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I1T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I2T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I3T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I4T12,
            }
            ,
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T5,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T6,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T7,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T8,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T9,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T10,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T11,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect {0}",
                  _requirementResource = BoosterType.I5T12,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I1,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I2,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I3,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I4,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.TRADE_COMRADE,
                  _requirementStringFormat = "Trade {0}",
                  _requirementResource = BoosterType.I5,
            },

            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect Cards",
                  _requirementResource = BoosterType.CARDS,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.COLLECT_RESOURCE,
                  _requirementStringFormat = "Collect Science",
                  _requirementResource = BoosterType.SCIENCE,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.SPEND_SCIENCE,
                  _requirementStringFormat = "Spend science",
                  _requirementResource = BoosterType.SCIENCE,
            },
            new MissionConfigAsset()
            {
                 _name = "",
                 _requirementType = MissionRequirementType.UPGRADE_CARDS,
                  _requirementStringFormat = "Upgrade Cards",
                  _requirementResource = BoosterType.CARDS,
            }
        };

        MissionConfigAssets.Instance.configs = configs;
    }


    #endregion

}

[System.Serializable]
public class EventDataConfig
{
    public string _eventName;
    public EventID _eventID;

    [Header("UI Prop")]
    public Color _blendColor;

    [Space(10)]
    [Header("Core Config")]
    public double _timeLength = -1;

    [Space(10)]
    public double _multiplierComrade;
    public double _initialComrade;

    [Space(10)]
    public List<IndustryConfig> _industryConfigs;

    [Space(10)]
    public RankConfigs _rankConfigs;

    private List<GeneratorConfig> _generatorToList;
    public List<GeneratorConfig> GeneratorToList
    {
        get
        {
            if (_generatorToList == null || _generatorToList.Count == 0)
            {
                _generatorToList = new List<GeneratorConfig>();
                foreach (IndustryConfig ins in this._industryConfigs)
                    _generatorToList.AddRange(ins._generatorConfigs);
            }
            return _generatorToList;
        }
    }

    public IndustryConfig FirstIndustry => this._industryConfigs[0];
    public GeneratorConfig FirstGenerator => FirstIndustry._generatorConfigs[0];

    public IndustryConfig GetIndustry(BoosterType _id)
    {
        return this._industryConfigs.Find(x => x._typeID == _id);
    }
    public GeneratorConfig GetGenerator(BoosterType _id, BoosterType _industryID = BoosterType.NONE)
    {
        if (_industryID != BoosterType.NONE)
        {
            IndustryConfig corresIns = GetIndustry(_industryID);

            if (corresIns.IsHave(_id, out GeneratorConfig result))
            {
                return result;
            }
        }

        //nếu user ko truyền industry ID hoặc id Ins truyền vào ko contain result

        foreach(IndustryConfig _ins in this._industryConfigs)
        {
            if (_ins.IsHave(_id, out GeneratorConfig result))
            {
                return result;
            }
        }

        Debug.LogError("WE CAN NOT FIND YOUR REQUIRE ID " + _id);
        return null;
    }
}

[System.Serializable]
public class IndustryConfig
{
    public string _industryName;
    public BoosterType _typeID;
    public Sprite _sprIcon;

    public bool _firstIndustry;
    [Space(10)]
    public double _comradeInitial;
    [Space(10)]
    public BoosterCommodity _cost2Unlock;
    [Space(10)]
    [HideInInspector]
    public int indexIndustry;
    [Space(10)]
    public List<GeneratorConfig> _generatorConfigs;

    public bool IsFree2Unlock => this._cost2Unlock == null || _cost2Unlock.type == BoosterType.NONE;

    public GeneratorConfig NextOf(BoosterType _type)
    {
        int index = _generatorConfigs.FindIndex(x => x._typeID == _type);
        if(index >= 0 && index < _generatorConfigs.Count - 1)
        {
            return _generatorConfigs[index + 1];
        }
        return null;
    }
    public GeneratorConfig PreviousOf(BoosterType _type)
    {
        int index = _generatorConfigs.FindIndex(x => x._typeID == _type);
        if (index > 0 && index < _generatorConfigs.Count)
        {
            return _generatorConfigs[index - 1];
        }
        return null;
    }
    public int IndexOf(BoosterType _type)
    {
        return  _generatorConfigs.FindIndex(x => x._typeID == _type);
    }

    public GeneratorConfig GetGenerator( BoosterType _id)
    {
        return this._generatorConfigs.Find(x => x._typeID == _id);
    }

    public bool IsHave(BoosterType _type, out GeneratorConfig resultIfContain)
    {
        resultIfContain = GetGenerator(_type);
        return resultIfContain != null;
    }
}

[System.Serializable]
public class GeneratorConfig
{
    public string _industryName;

    public BoosterType _typeID;
    public Sprite _sprIcon;
    [Space(10)]
    public BoosterCommodity _profit;
    public double _intervalProfit;
    [Space(10)]
    public BoosterCommodity _cost2Unlock;
    public List<BoosterCommodity> _cost2BuyOne;
    [Space(10)]
    public int researcherID;
    public int sciencePerObjecttive;
    [HideInInspector]
    public int indexIndustry;


    public bool IsFree2Unlock => this._cost2Unlock == null || _cost2Unlock.type == BoosterType.NONE;


}