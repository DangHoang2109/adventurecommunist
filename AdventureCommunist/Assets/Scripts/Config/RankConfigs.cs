using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
[System.Serializable]
public class RankConfigs
{
    public EventID _eventID;
    public RankConfig[] _ranks;

    public RankConfig GetRank(int index)
    {
        if (index < 0 || index >= _ranks.Length)
            return null;

        return _ranks[index];
    }
    public RankConfig NextRank(int currentIndex)
    {
        return GetRank(currentIndex + 1);
    }
}
[System.Serializable]
public class RankConfig
{
    public int _rankID;

    public Sprite _sprIconRank;
    public Sprite _sprIconAvatar;

    public ChestCommodity _rewardCompleteRank;


    public MissionConfig[] _missions;
    public int AmountMission => _missions.Length;
    public List<MissionConfig> listMission => _missions.ToList();

}
