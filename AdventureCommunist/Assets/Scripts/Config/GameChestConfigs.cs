﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ChestID
{
    //Motherland chest
    NONE = -1,
    WOODEN_CHEST = 10, //thùng cùi nhất, hay dùng free và mission liên quan đến resource
    STONE_CHEST = 11, //thùng vừa, hay gift thep pattern trong shop và mission liên quan đến science/card
    SILVER_CHEST = 12, //thùng tốt, hay gift khi rank up

    OPERATION_CHEST = 13, //reward trong operation battle pass

    IRON_CHEST = 14, //thùng mua gold, không drop
    GOLD_CHEST = 15, //thùng iap, không drop
    DIAMOND_CHEST = 16, //thùng iap, không drop

    RARE_CHEST = 20, //thùng chỉ drop ra card rare
    EPIC_CHEST = 21, //thùng chỉ drop ra 5 card epic
    SUPREME_CHEST = 22, //thùng chỉ drop ra 1 card supreme

    //Event Chest

    //Leaderboard chest
}
[CreateAssetMenu(menuName = "Configs/Game/GameChestConfigs", fileName = "GameChestConfigs")]
public class GameChestConfigs : ScriptableObject
{
    public static GameChestConfigs Instance
    {
        get
        {
            return LoaderUtility.Instance.GetAsset<GameChestConfigs>("Game/Config/GameChestConfigs");
        }
    }

    public List<ChestConfig> ChestConfigs;

    public ChestConfig GetChestConfig(ChestID _chestiD)
    {
        return ChestConfigs.Find(x => x.chestID == _chestiD);
    }

}

[System.Serializable]
public class ChestConfig
{
    public ChestID chestID;
    public BoosterCommodity priceBuyInShop;

    //UI Config, để thêm vào nếu cần
    public string name;
    public Sprite sprIcon;
    
}
[System.Serializable]
public class CardPairTierAmount
{
    public CardTierID tier;
    public int amount;

    public CardPairTierAmount()
    {

    }
    public CardPairTierAmount(CardPairTierAmount c)
    {
        tier = c.tier;
        amount = c.amount;
    }
}
[System.Serializable]
public class EventChestRewardConfig
{
    public ChestID chestID;

    [Header("Prop for reward generate")]
    [Tooltip("This is amount of randomly slot, not count the quarantee")]
    public int amountSlotCard;
    [Space(5f)]
    [Header("Prop for reward contain")]
    public int totalCard;
    public int elixirMin;
    public int elixirMax;

    public List<CardPairTierAmount> quaranteeCards;

    /// <summary>
    /// Only use in event
    /// Always be 0 in MainGame
    /// Can be 0 or higher in Event
    /// </summary>
    public int crowns;

    [System.NonSerialized]
    private ChestConfig _chestConfig;
    public ChestConfig ChestConfig
    {
        get
        {
            if (_chestConfig == null || _chestConfig.chestID != this.chestID)
                _chestConfig = GameChestConfigs.Instance.GetChestConfig(this.chestID);
            return _chestConfig;
        }
    }


    public int RandomElixir()
    {
        return Random.Range(this.elixirMin, this.elixirMax + 1);
    }

}
[System.Serializable]
public class EventChestUsageConfig
{
    public ChestID chestID;
    public List<EventChestRewardConfig> stepConfigByIndex;
    
    public EventChestRewardConfig GetRewardByStep(int currentStepIndex)
    {
        if (currentStepIndex >= 0 && currentStepIndex < this.stepConfigByIndex.Count)
            return this.stepConfigByIndex[currentStepIndex];
        return null;
    }

    [System.NonSerialized]
    private ChestConfig _chestConfig;
    public ChestConfig ChestConfig
    {
        get
        {
            if (_chestConfig == null || _chestConfig.chestID != this.chestID)
                _chestConfig = GameChestConfigs.Instance.GetChestConfig(this.chestID);
            return _chestConfig;
        }
    }

    public EventChestRewardConfig GetRewardConfig(int _index)
    {
        if (_index >= 0 && _index < this.stepConfigByIndex.Count)
            return this.stepConfigByIndex[_index];

        return null;
    }
}