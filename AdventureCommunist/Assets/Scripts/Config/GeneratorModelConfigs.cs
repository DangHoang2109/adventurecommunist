using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Configs/Game/GeneratorModelConfigs", fileName = "GeneratorModelConfigs")]
public class GeneratorModelConfigs : ScriptableObject
{
    public static GeneratorModelConfigs Instance
    {
        get
        {
            return LoaderUtility.Instance.GetAsset<GeneratorModelConfigs>("Game/Config/GeneratorModelConfigs");
        }
    }

    public List<GeneratorEventUsageConfig> ConfigForEachEvent;

    public GeneratorAssetConfig GetAsset(EventID _eventID, BoosterType _type)
    {
        return this.ConfigForEachEvent.Find(x => x._eventID == _eventID)?.GetAsset(_type) ;
    }
    public GeneratorAssetConfig GetAsset(BoosterType _type)
    {
        return this.ConfigForEachEvent.Find(x => x._eventID == IddleGameManager.Instance.CurrentSceneHandler._id)?.GetAsset(_type);
    }

    [SerializeField]
    private EventID _editorEventID;
    [ContextMenu("Clone Asset from Map Config")]
    private void Editor_CloneAssetFromMap()
    {
        EventDataConfig _eventConfig = GameEventDataConfigs.Instance.GetConfig(_editorEventID);

        List<Sprite> spriteAssetInDB = GameUtils.LoadAllAssetsInFolder<Sprite>("Assets/Image/Resource/", new List<string> { "*.png", "*.jpg"});
        string format = "icon-{0}";

        GeneratorEventUsageConfig _usage = new GeneratorEventUsageConfig()
        {
            _eventID = _eventConfig._eventID,
            _asset = new List<GeneratorAssetConfig>()
        };

        if (ConfigForEachEvent.Find(x => x._eventID == _editorEventID) != null)
            ConfigForEachEvent.Remove(ConfigForEachEvent.Find(x => x._eventID == _editorEventID));

        foreach (IndustryConfig _industry in _eventConfig._industryConfigs)
        {
            string indNameFormated = string.Format(format, _industry._industryName.ToLower().Replace(" ", ""));
            Sprite indSpr = spriteAssetInDB.Find(x => x.name.Equals(indNameFormated));
            if (indSpr == null)
                Debug.LogError("KHONG TIM THAY ASSET CUA GEN " + indNameFormated);
            else
            {
                _usage._asset.Add(new GeneratorAssetConfig()
                {
                    _id = _industry._typeID,
                    _name = _industry._industryName,
                    _sprIcon = indSpr
                });
            }


            foreach (GeneratorConfig _gen in _industry._generatorConfigs)
            {
                string genNameFormated = string.Format(format, _gen._industryName.ToLower().Substring(0, _gen._industryName.Length - 1).Replace(" ", ""));
                //format the gen name, combine to get path
                Sprite spr = spriteAssetInDB.Find(x => x.name.Equals(genNameFormated));
                if (spr == null)
                    Debug.LogError("KHONG TIM THAY ASSET CUA GEN " + genNameFormated);
                else
                {
                    _gen._sprIcon = spr;

                    _usage._asset.Add(new GeneratorAssetConfig()
                    {
                        _id = _gen._typeID,
                        _name = _gen._industryName,
                        _sprIcon = spr
                    });
                }
            }
        }

        ConfigForEachEvent.Add(_usage);
    } 
}
[System.Serializable]
public class GeneratorEventUsageConfig
{
    public EventID _eventID;
    public List<GeneratorAssetConfig> _asset;

    public GeneratorAssetConfig GetAsset(BoosterType _id)
    {
        return this._asset.Find(x => x._id == _id);
    }
}

[System.Serializable]
public class GeneratorAssetConfig
{
    public BoosterType _id;

    public string _name;
    public Sprite _sprIcon;
}
