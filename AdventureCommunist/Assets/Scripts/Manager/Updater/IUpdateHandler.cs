﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IUpdateHandler : MonoBehaviour
{
    /// <summary>
    /// Đặt nội dung hàm update vào đây
    /// Không dùng hàm update của Monobehavior trong object này
    /// Sẽ có manager gọi hàm OnUpdate này dùm cho sau khi gọi AssignToUpdateManager
    /// </summary>
    public virtual void OnUpdate()
    {

    }

    /// <summary>
    /// Assign vào update manager
    /// </summary>
    public virtual void AssignToUpdateManager()
    {
        UpdaterManager.Instance.AssignMember(this);
    }


    /// <summary>
    /// Unassign khỏi update manager
    /// </summary>
    public virtual void UnAssignToUpdateManager()
    {
        UpdaterManager.Instance.UnAssignMember(this);
    }
}
