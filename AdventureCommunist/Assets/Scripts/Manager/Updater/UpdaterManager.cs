﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdaterManager : MonoSingleton<UpdaterManager>
{
    [SerializeField]
    private List<IUpdateHandler> updateMembers;
    private Dictionary<int, IUpdateHandler> dicUpdateMembers;

    private List<IUpdateHandler> pre_AssignMember;
    private bool isRegisterAssign;
    private List<IUpdateHandler> pre_UnAssignMember;
    private bool isRegisterUnAssign;

    private bool isActive;

    public bool IsActive { get => isActive; set => isActive = value; }

    public void OnStart()
    {
        IsActive = true;
    }
    public void OnChangeScene()
    {
        LoadData();
    }
    public override void Init()
    {
        base.Init();
        LoadData();
    }
    private void LoadData()
    {
        this.updateMembers = new List<IUpdateHandler>();
        this.dicUpdateMembers = new Dictionary<int, IUpdateHandler>();

        pre_AssignMember = new List<IUpdateHandler>();
        pre_UnAssignMember = new List<IUpdateHandler>();

        isRegisterAssign = false;
        isRegisterUnAssign = false;
    }

    public void AssignMember(IUpdateHandler newMember)
    {
        if (dicUpdateMembers == null)
            dicUpdateMembers = new Dictionary<int, IUpdateHandler>();
        if (updateMembers == null)
            updateMembers = new List<IUpdateHandler>();

        if (!dicUpdateMembers.ContainsKey(newMember.GetInstanceID()))
        {
            pre_AssignMember.Add(newMember);
        }
        else
        {
            Debug.LogError("ĐÃ ASSIGN OBJECT NÀY");
        }
    }

    private void HandlePreAssignMember()
    {
        foreach (IUpdateHandler _m in pre_AssignMember)
        {
            TrulyAssignMember(_m);
        }
        pre_AssignMember.Clear();
    }
    private void TrulyAssignMember(IUpdateHandler newMember)
    {
        dicUpdateMembers.Add(newMember.GetInstanceID(), newMember);
        updateMembers.Add(newMember);
    }

    public void UnAssignMember(IUpdateHandler member)
    {
        if (dicUpdateMembers == null)
            dicUpdateMembers = new Dictionary<int, IUpdateHandler>();
        if (updateMembers == null)
            updateMembers = new List<IUpdateHandler>();

        if (dicUpdateMembers.ContainsKey(member.GetInstanceID()))
        {
            pre_UnAssignMember.Add(member);
        }
    }
    private void HandlePreUnAssignMember()
    {
        foreach (IUpdateHandler _m in pre_UnAssignMember)
        {
            TrulyUnAssignMember(_m);
        }
        pre_UnAssignMember.Clear();
    }
    private void TrulyUnAssignMember(IUpdateHandler member)
    {
        dicUpdateMembers.Remove(member.GetInstanceID());
        updateMembers.Remove(member);
    }



    private void LateUpdate()
    {
        if (updateMembers != null && updateMembers.Count > 0)
        {
            for (int i = 0; i < updateMembers.Count; i++)
            {
                if (updateMembers[i] == null)
                    UnAssignMember(updateMembers[i]);
                else
                    updateMembers[i]?.OnUpdate();
            }
        }

        HandlePreAssignMember();
        HandlePreUnAssignMember();
    }

    public void SkipTimeAllMemeber(double timeSkip)
    {
        if (updateMembers != null && updateMembers.Count > 0)
        {
            for (int i = 0; i < updateMembers.Count; i++)
            {
                if (updateMembers[i] == null)
                    UnAssignMember(updateMembers[i]);

                //if (updateMembers[i] != null && updateMembers[i] is IDefendObject)
                //{
                //    (updateMembers[i] as IDefendObject)?.SkipTime(timeSkip);
                //}
            }
        }
    }

    public void SkipTimeAllGeneratorMemeber(double timeSkip)
    {
        if (updateMembers != null && updateMembers.Count > 0)
        {
            for (int i = 0; i < updateMembers.Count; i++)
            {
                if (updateMembers[i] == null)
                    UnAssignMember(updateMembers[i]);

                if (updateMembers[i] != null && updateMembers[i] is BaseGenerator)
                {
                    (updateMembers[i] as BaseGenerator)?.SkipTime(timeSkip);
                }
            }
        }
    }
}
