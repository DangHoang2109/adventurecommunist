﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ButtonUpgradeComrade : MonoBehaviour
{
    private UserGameComradeBoosterData _comradeData;

    public TextMeshProUGUI _tmpAddValue;

    [Space(10)]
    public TextMeshProUGUI _tmpNextCost;
    public Image _fillToCost;

    [Space(10)]
    public GameObject _gButtonBuy;
    public NotiBadge _badge;
    public TextMeshProUGUI _tmpButtonCostBuy;

    private int _amountCanBuy = 0;
    private BulkBuyType _buyType;
    public System.Action _OnClickedUpgradeSuccess;

    private void OnValidate()
    {
        this._badge = GetComponentInChildren<NotiBadge>();

    }

    public void OnInit(UserGameComradeBoosterData _data)
    {
        _comradeData = _data;

        IddleGameManager.Instance.CurrentBoosterData.AddCallbackBooster(_data._industryID, this.OnChangeBooster);
    }
    public void OnChangeBooster(BoosterCommodity _booster)
    {
        _amountCanBuy = GameUtils.GetMaxItemCanBuyByIddleFormular(_booster.GetValueDouble(), _comradeData.ConfigInitialCostComrade, _comradeData.ConfigMultiplierComrade, _comradeData._amountTimeBought);

        //đủ mua
        _badge.SetText(_amountCanBuy);
        _gButtonBuy.SetActive(_amountCanBuy > 0);
        _tmpButtonCostBuy.SetText(
            _buyType == BulkBuyType.ONE? 
            _comradeData._nextCost.GetIddleUnitString : 
            _comradeData.GetCostWithNextNStep(_amountCanBuy).ToIddleUnitString()
            );

        //không đủ mua item nào
        _tmpNextCost.SetText($"Save {_comradeData._nextCost.GetIddleUnitString} {_comradeData._industryID}");
        _fillToCost.fillAmount = (float)(_booster.GetValueDouble() / _comradeData._nextCost.GetValueDouble());
    }
    public void OnChangeBulkType(BulkBuyType type)
    {
        _buyType = type;

        _tmpButtonCostBuy.SetText(
        _buyType == BulkBuyType.ONE ?
        _comradeData._nextCost.GetIddleUnitString :
        _comradeData.GetCostWithNextNStep(_amountCanBuy-1).ToIddleUnitString()
        );
    }

    public void OnClickBuy()
    {
        if(_amountCanBuy > 0)
        {
            _comradeData.OnUpgradeComradeBought(_buyType  == BulkBuyType.ONE ? 1 : _amountCanBuy);
            OnChangeBooster(IddleGameManager.Instance.CurrentBoosterData.GetBoosterCommodity(this._comradeData._industryID));

            _OnClickedUpgradeSuccess?.Invoke();
        }
    }
}
