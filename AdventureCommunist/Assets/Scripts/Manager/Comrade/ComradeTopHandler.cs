using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComradeTopHandler : IUpdateHandler
{
    public ITimerUI _progressUI;

    private float Interval = 1f;
    private float _timeHaveWait = 0f;

    private bool IsCanCollect => _timeHaveWait >= Interval;

    private UserGameBoosterData _userBoosterData;

    public virtual void AssignCallback()
    {
        SetUpProp();
        this.AssignToUpdateManager();
        this._userBoosterData.AddCallbackBooster(BoosterType.COMRADE, this.OnChangeComradeAmount);
    }
    public virtual void UnAssignCallback()
    {
        UnAssignToUpdateManager();
        this._userBoosterData.RemoveCallbackBooster(BoosterType.COMRADE, this.OnChangeComradeAmount);
    }
    private void OnChangeComradeAmount(BoosterCommodity b)
    {
        this._progressUI.SetExtraText(b.GetIddleUnitString);
    }


    private void SetUpProp()
    {
        this._userBoosterData = IddleGameManager.Instance.CurrentBoosterData;
        this._timeHaveWait = 0;

        BoosterCommonAssetConfig _assetComrade = GameAssetsConfigs.Instance.GetEventAssetConfig(IddleGameManager.Instance.CurrentSceneID).GetAsset(BoosterType.COMRADE);
        this._progressUI.SetExtraIcon(_assetComrade._iconBooster);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        //set value of timer
        _timeHaveWait += Time.deltaTime;

        if (IsCanCollect)
        {
            _timeHaveWait = 0;
            this._userBoosterData.AddComradeBySecond(Interval);
        }

        this._progressUI.SetTimer(this._timeHaveWait, Interval);
    }
}
