using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComradeBotButtonHanler : BaseButtonHandler
{

    public override void OnClick()
    {
        base.OnClick();
    }

    public override void OnShow()
    {
        base.OnShow();
        UpgradeComradeDialog dialog = GameManager.Instance.OnShowDialogWithSorting<UpgradeComradeDialog>("GUI/Dialogs/UpgradeComradeDialog", PopupSortingType.BellowBottomBar);
        dialog.OnClosed += this.OnHide;
    }



    public override void OnHide()
    {
        base.OnHide();
    }
}
