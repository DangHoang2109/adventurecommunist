using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoSingleton<GameSceneManager>
{
    public ButtonBulkBuy _buttonBulkBuy;

    public GeneratorTabBase _generatorTabBase;

    public RectTransform _canvas;

    public BotBarUIManager _botBarUI;
    public TopUIManager _topBarUI;

    public float ScreenWidth => _canvas.sizeDelta.x;

    private void Start()
    {
        this._botBarUI.OnInit();
        _topBarUI.OnInit();
    }
    public void OnCompleteLoadData()
    {
        _generatorTabBase.OnCompleteLoadData();
    }
}
