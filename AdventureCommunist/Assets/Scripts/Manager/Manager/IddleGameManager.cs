﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// Manager quản lý các common của game Iddle
/// Mỗi scene: Main Game và Event sẽ có IddleEventStatManager riêng
/// Lưu trong 1 dictioonary
/// Note: Chỉ 
/// </summary>
public class IddleGameManager : MonoSingleton<IddleGameManager>
{
    public Dictionary<EventID, IddleEventStatManager> dicDataStat;

    [SerializeField]
    private EventID _currentSceneID;
    public EventID CurrentSceneID => _currentSceneID;

    private bool _isLoadSceneByNextMine = false;

    public IddleEventStatManager CurrentSceneHandler
    {
        get
        {
            if (dicDataStat.ContainsKey(_currentSceneID))
                return dicDataStat[_currentSceneID];

            Debug.LogError($"NO CONTAIN ID {_currentSceneID} IN DIC");
            return null;
        }
    }
    //public UserGameCardChestData CurrentCardChestData
    //{
    //    get
    //    {
    //        if (CurrentSceneHandler.CardChestData == null)
    //            CurrentSceneHandler.CardChestData = UserGameDatas.Instance.GetData(CurrentSceneID)?.CardChest;

    //        return CurrentSceneHandler.CardChestData;
    //    }
    //}
    public UserGameBoosterData CurrentBoosterData
    {
        get
        {
            return CurrentSceneHandler.BoosterData;
        }
    }
    public UserGameMapData CurrentMapData
    {
        get
        {
            return CurrentSceneHandler.MapData;
        }
    }
    //public UserDeliveryDropData CurrentDeliveryData
    //{
    //    get
    //    {
    //        if (CurrentSceneHandler.DeliveryDropData == null)
    //            CurrentSceneHandler.DeliveryDropData = UserGameDatas.Instance.GetData(CurrentSceneID)?.Delivery;

    //        return CurrentSceneHandler.DeliveryDropData;
    //    }
    //}
    //public UserGameStoreDatas CurrentStoreData
    //{
    //    get
    //    {
    //        if (CurrentSceneHandler.StoreData == null)
    //            CurrentSceneHandler.StoreData = UserGameDatas.Instance.GetData(CurrentSceneID)?.Store;

    //        return CurrentSceneHandler.StoreData;
    //    }
    //}
    public EventDataConfig CurrentEventConfig
    {
        get
        {
            return CurrentSceneHandler.EventDataConfig;
        }
    }

    /// <summary>
    /// Parse data hiện có lên dictionary
    /// 
    /// </summary>
    public void OnUserOpenApp()
    {
        dicDataStat = new Dictionary<EventID, IddleEventStatManager>();

        dicDataStat.Add(EventID.MOTHERLAND, new IddleEventStatManager(EventID.MOTHERLAND));
    }

    public void OnUserChangeScene(EventID _id)
    {
        this._currentSceneID = _id;

        //StatBoostManager.Instance.OnUserChangeScene(_id);
        //ChestCardManager.Instance.OnUserChangeScene(_id);

        StartCoroutine(ieLoadData(_id));
    }

    private IEnumerator ieLoadData(EventID _id)
    {
        CurrentSceneHandler.OnUserLoginEvent();
        UserGameData data = UserGameDatas.Instance.GetData(_id);

        yield return new WaitForEndOfFrame(); //WaitForSeconds(0.1f);
        //GoblinTutorialManager.Instance.InitTutorial(data.Tutorial);
        Debug.Log("edit this");

        double totalOffline = data.OnUserOnline();
        data.booster.OnUserOnline(totalOffline);

        double offlineComrade = SkipTime(_id, totalOffline);
        //    //Offline quá ít thì ko show dialog mà add thẳng vào luôn
        //    if (totalOffline > 10)
        //    {
        //        OfflineRewardDialog dialog =
        //GameManager.Instance.OnShowDialog<OfflineRewardDialog>("GUI/Dialogs/OfflineRewardDialog");
        //        dialog?.ParseData(mapIDToString, totalOffline, offlineProfit);
        //    }
        //    else
        //        AddBooster(BoosterType.COIN, offlineProfit);

        CurrentBoosterData.AddBooster(BoosterType.COMRADE, offlineComrade);

        //if (!_isLoadSceneByNextMine)
        //{
        //    double totalOffline = data.OnUserOnline();

        //    double totalOfflineWithX2Bonus = data.booster.OnUserOnline(totalOffline);

        //    string mapIDToString = "";
        //    if (data.IsMainGameData)
        //        mapIDToString = $"Mine {data.CurrentRewardIndex + 1}";
        //    else
        //        //event
        //        mapIDToString = EventDataConfigs.Instance.GetConfig(_id)?._eventName;

        //    SkipTime(_id, totalOffline);

        //    double offlineProfit = CurrentSceneHandler.GetTotalAutoProfitByTime(totalOffline) + CurrentSceneHandler.GetTotalAutoProfitByTime(totalOfflineWithX2Bonus * 2);

        //    //your need data in here:
        //    //totalOffline: thời gian offline bằng second
        //    //offlineProfit: thu nhập khi offline
        //    //mapIDToString: title name của map offline
        //    //show offline dialog in here, yield wait more if needed
        //    //Offline quá ít thì ko show dialog mà add thẳng vào luôn
        //    if (totalOffline > 10)
        //    {
        //        OfflineRewardDialog dialog =
        //GameManager.Instance.OnShowDialog<OfflineRewardDialog>("GUI/Dialogs/OfflineRewardDialog");
        //        dialog?.ParseData(mapIDToString, totalOffline, offlineProfit);
        //    }
        //    else
        //        AddBooster(BoosterType.COIN, offlineProfit);
        //}

        //_isLoadSceneByNextMine = false;

        yield return new WaitForEndOfFrame();
        //GoblinActionCallbackManager.Instance.OnInvokeMineMapLoaded();
        GameSceneManager.Instance.OnCompleteLoadData();
    }

    public void OnAppFocus(bool isFocus)
    {
        if (!isFocus)
        {
            Debug.Log($"On app focus {isFocus}, save game");
            CurrentSceneHandler?.SaveGameData();
        }
    }

    //using on wheel and shop
    public double SkipTimeCurrent(double time)
    {
        if (time <= 0)
            return 0;

        return CurrentSceneHandler.SkipTime(time);
    }

    public double SkipTime(EventID _id, double time)
    {
        if (time <= 0)
            return 0;

        if (dicDataStat.ContainsKey(_id))
        {
            return dicDataStat[_id].SkipTime(time);
        }
        else
        {
            Debug.LogError("NO ACTIVE EVENT " + _id);
            return 0;
        }
    }
    //using on editor online
    public void SkipTimeAllEvent(double time)
    {
        if (time <= 0)
            return;

        foreach (IddleEventStatManager stat in this.dicDataStat.Values)
            stat.SkipTime(time);
    }

    public void OnAGeneratorUnlock(BaseGenerator obj)
    {
        CurrentSceneHandler.OnAGeneratorUnlock(obj);
    }

    /// <summary>
    /// Call when a mine is auto clicked
    /// </summary>
    /// <param name="obj"></param>
    public void OnAGeneratorAutomated(BaseGenerator obj)
    {
        CurrentSceneHandler.OnAGeneratorAutomated(obj);
    }

    #region Booster
    //public void AddBooster(BoosterType type, double value)
    //{
    //    this.CurrentBoosterData.AddValueBooster(type, value);
    //}
    //public void AddBooster(BoosterCommodity b)
    //{
    //    this.CurrentBoosterData.AddValueBooster(b);
    //}
    //public BoosterCommodity Usebooster(BoosterType type, double value)
    //{
    //    return this.CurrentBoosterData.UseBooster(type, value);
    //}
    //public bool IsHaveBooster(BoosterType type, double value)
    //{
    //    return this.CurrentBoosterData.IsHasBooster(type, value);
    //}
    //public BoosterCommodity SetValueBooster(BoosterType type, double value)
    //{
    //    return this.CurrentBoosterData.SetValueBooster(type, value);
    //}

    //public BoosterCommodity GetValueBooster(BoosterType type)
    //{
    //    return this.CurrentBoosterData.GetBoosterCommodity(type);
    //}

    #endregion

    #region Reward Drop

    private bool isInTutorial;
    public void SetInTutorial(bool _isInTutorial)
    {
        this.isInTutorial = _isInTutorial;
    }
    #endregion

    //public int CurrentMapIndex => UserGameDatas.Instance.GetData(this.CurrentSceneID).currentRewardIndex + 1;
    /// <summary>
    /// Set up data và đi đến index tiếp theo
    /// Tại Main Game: Gọi khi destroy và click active Final Gate
    /// Tại Event: Gọi khi destroy và active Checkpoint Gate hoặc Mine
    /// What to do:
    /// Tại Main Game:  Tăng reward index (chest affect), Clear Booster Data (coin and elixir affect), clear map save data (map affected),
    /// Tại Event: Tăng reward index thôi
    /// </summary>
    //public List<CardData> GoToNextRewardIndex()
    //{
    //    UpdaterManager.Instance.OnChangeScene();
    //    StatBoostManager.Instance.ClearAllCallback();

    //    if (CurrentSceneHandler.IsMainGameData)
    //    {
    //        _isLoadSceneByNextMine = true;
    //        return CurrentSceneHandler.GoToNextRewardIndex_MainGame();
    //    }
    //    else
    //        return CurrentSceneHandler.GoToNextRewardIndex_Event();
    //}
    public void CompleteGoNextIndex()
    {
        LoadingManager.Instance.ShowLoadingNextMine(1);
        GameManager.Instance.OnLoadScene(SceneName.MAIN_GAME);
    }
}

[System.Serializable]
public class IddleEventStatManager
{
    public EventID _id;

    private EventDataConfig _eventConfig;

    private UserGameBoosterData _boosterData;
    private UserGameMapData _mapData;

    //private UserGameCardChestData _CardChestData;
    //private UserDeliveryDropData _DeliveryData;
    //private UserGameWheelData _WheelData;
    //private UserGameStoreDatas _StoreData;

    public UserGameBoosterData BoosterData
    {
        get
        {
            if (_boosterData == null)
                _boosterData = UserGameDatas.Instance.GetData(this._id)?.Booster;
            return _boosterData;
        }
    }
    public UserGameMapData MapData
    {
        get
        {
            if (_mapData == null)
                _mapData = UserGameDatas.Instance.GetData(this._id)?.Map;
            return _mapData;
        }
    }
    public bool IsMainGameData => _id == EventID.MOTHERLAND;
    public bool IsEventData => _id != EventID.MOTHERLAND;
    public EventDataConfig EventDataConfig
    {
        get
        {
            if (_eventConfig == null)
                this._eventConfig = GameEventDataConfigs.Instance.GetConfig(this._id);
            return _eventConfig;
        }
    }
    private void AssignBoostingCallback()
    {

    }

    /// <summary>
    /// Vào event lần đầu tiên 
    /// </summary>
    public void OnUserStartEvent()
    {

    }
    /// <summary>
    /// Mỗi lần load vào scene event
    /// </summary>
    public void OnUserLoginEvent()
    {
        //set config property
        SetConfigProp();
        //load save data lên
        LoadSaveData();

        //assign Need callback
        AssignBoostingCallback();
    }

    #region Init Config Prop and Load save game

    private void SetConfigProp()
    {

    }
    private void SetBoosterInitialValue()
    {
    }

    private void LoadSaveData()
    {
        //UserGameDatas.Instance.OpenMap(this._id);

        SetBoosterInitialValue();
    }


    public void SaveGameData()
    {
        UserGameDatas.Instance.SaveGameData(_id);
    }
    #endregion  Init Config Prop and Load save game

    /// <summary>
    /// Set up data và đi đến index tiếp theo
    /// Tại Main Game: Gọi khi destroy và click active Final Gate
    /// Tại Event: Gọi khi destroy và active Checkpoint Gate hoặc Mine
    /// What to do:
    /// Tại Main Game:  Tăng reward index (chest affect), Clear Booster Data (coin and elixir affect), clear map save data (map affected),
    /// Tại Event: Tăng reward index thôi
    /// </summary>
    //public List<CardData> GoToNextRewardIndex_MainGame()
    //{
    //    this.AmountTimeBought = 0;
    //    this._currentGoblinOnField = 0;

    //    if (this.autoProfitGenerators == null)
    //        autoProfitGenerators = new List<IProfitableObject>();
    //    else
    //        this.autoProfitGenerators.Clear();

    //    if (this.activeProfitGenerators == null)
    //        activeProfitGenerators = new List<IProfitableObject>();
    //    else
    //        this.activeProfitGenerators.Clear();

    //    return UserGameDatas.Instance.GetData(this._id)?.GoToNextRewardIndex();
    //}
    //public List<CardData> GoToNextRewardIndex_Event()
    //{
    //    return UserGameDatas.Instance.GetData(this._id)?.GoToNextRewardIndex();
    //}



    #region Profit
    /// <summary>
    /// Các mine đã unlock
    /// Gồm mine auto và ko auto
    /// </summary>
    public List<BaseGenerator> activeProfitGenerators;

    /// <summary>
    /// Array các mine auto
    /// cache lại mỗi khi active Profit thay đổi để tối ưu performance
    /// </summary>
    public List<BaseGenerator> autoProfitGenerators;

    public void OnAGeneratorUnlock(BaseGenerator obj)
    {
        if (this.activeProfitGenerators == null)
            activeProfitGenerators = new List<BaseGenerator>();

        activeProfitGenerators.Add(obj);
    }

    /// <summary>
    /// Call when a mine is auto clicked
    /// </summary>
    /// <param name="obj"></param>
    public void OnAGeneratorAutomated(BaseGenerator obj)
    {
        if (this.autoProfitGenerators == null)
            autoProfitGenerators = new List<BaseGenerator>();

        autoProfitGenerators.Add(obj);
    }
    #endregion Profit

    #region Comrade
    public double GetTotalComradeByTime(double _timeSkip)
    {
        return BoosterData.CurrentComradeIncomePerSec * _timeSkip;
    }
    #endregion

    public IddleEventStatManager()
    {

    }
    public IddleEventStatManager(EventID _i)
    {
        this._id = _i;
    }

    /// <summary>
    /// Hàm skip thời gian, forward đi
    // Dùng khi có các skip time booster (wheel, bottle)
    // Dùng khi user login lại sau khi offline


    //Check spawn delivery : Max 1 if time > DeliveryInterval
    //Check spawn free goblin (Time / intervalWait clamp limit gob)
    //Add profit: time * ProfitPerSec
    //Give goblin the time for them to go
    /// </summary>
    /// <param name="timeSkip"></param>
    /// <returns>amount of offline coin to show on dialog<returns>
    public double SkipTime(double timeSkip)
    {
        if (timeSkip <= 0)
            return 0;

        //delivery, wheel, shop has be call in OnUserOnline Function

        //Add profit: time * ProfitPerSec
        //Add to user Profile
        double _comradeOffline = this.GetTotalComradeByTime(timeSkip);


        //Subtract iprofit time if have been active
        //if complete interval then generator will add booster too
        UpdaterManager.Instance.SkipTimeAllGeneratorMemeber(timeSkip);

        return _comradeOffline;
    }

}

