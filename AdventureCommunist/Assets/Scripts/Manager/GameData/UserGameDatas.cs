﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using System;

[System.Serializable]
public class UserGameDatas
{
    public List<UserGameData> listDatas;
    public Dictionary<EventID, UserGameData> dicDatas;

    private static UserGameDatas _instance;
    public static UserGameDatas Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameDataManager.Instance.GameDatas.gameDatas;
            return _instance;
        }
    }
    
    public UserGameData GetData(EventID id)
    {
        if (dicDatas.ContainsKey(id))
            return dicDatas[id];

        Debug.LogError("NOT FIND DATA " + id);
        return null;
    }

    public void CreateUser()
    {
        listDatas = new List<UserGameData>();
        dicDatas = new Dictionary<EventID, UserGameData>();

        CreateNewData(EventID.MOTHERLAND);
    }
    public void OpenGame()
    {
        if(dicDatas == null)
            dicDatas = new Dictionary<EventID, UserGameData>();

        foreach(UserGameData g in this.listDatas)
        {
            if (!dicDatas.ContainsKey(g._id))
                dicDatas.Add(g._id, g);
        }

        //OpenMap(EventID.MOTHERLAND);
    }



    public void CreateNewData(EventID id)
    {
        if (!dicDatas.ContainsKey(id))
        {
            UserGameData _newDate = new UserGameData();
            _newDate.CreateUser(id);

            AddData(_newDate);
        }
    }

    public void AddData(UserGameData d)
    {
        this.listDatas.Add(d);
        this.dicDatas.Add(d._id, d);

        SaveData();
    }
    public void RemoveData(EventID id)
    {
        if (id == EventID.MOTHERLAND)
            Debug.LogError("YO REMOVE MAIN GAME? ARE U SURE?");

        if (dicDatas.ContainsKey(id))
        {
            UserGameData d = dicDatas[id];
            listDatas.Remove(d);
            dicDatas.Remove(id);

            SaveData();
        }
    }

    public void SaveGameData(EventID _id)
    {
        UserGameData _data = this.GetData(_id);

        _data.OnUserOffline();

        //save the map
        SaveMapData(_id);
    }
    public void SaveMapData(EventID _id)
    {
        this.GetData(_id)?.Map?.SaveData();

        SaveData();
    }

    private void SaveData()
    {
        GameDataManager.Instance.SaveUserData();
    }


}

[System.Serializable]
/// <summary>
/// Data for a single event or main game
///Mỗi data gồm:
///     - GameBoosterData
///         + UserCoin
///         + UserElixir
///         + UserCard;
///         + User wheel slot spins
///         + User chest free, chest wait time
///         + Key, Crown
///     - GameMapData
///         +tileStruture
/// </summary>
public class UserGameData
{
    public EventID _id;
    public int currentRewardIndex;
    public UserGameBoosterData booster;
    public UserGameMapData map;
    //public UserGameCardChestData cardChest;
    //public UserDeliveryDropData delivery;
    //public UserGameWheelData wheel;
    //public UserGameStoreDatas store;
    //public UserGameTutorialData tutorial;
    public bool IsMainGameData => _id == EventID.MOTHERLAND;
    public bool IsEventData => _id != EventID.MOTHERLAND;


    /// <summary>
    /// Dùng để tính thời gian offline khi user login
    /// Không thể dùng để log firebase data, vì có thể bị thay đổi khi user dùng skip booster trong lúc ở event khác
    /// </summary>
    public long lastTimeOnline;
    /// <summary>
    /// Thời gian ofline, thời gian đúng không bị thay đổi
    /// Dùng để log firebase nếu có
    /// </summary>
    public long lastTimeOnlineTruly;

    public int CurrentRewardIndex => currentRewardIndex;
    public UserGameBoosterData Booster => this.booster;
    public UserGameMapData Map => this.map;
    //public UserGameCardChestData CardChest => this.cardChest;
    //public UserDeliveryDropData Delivery => this.delivery;
    //public UserGameWheelData Wheel => this.wheel;
    //public UserGameStoreDatas Store => this.store;
    //public UserGameTutorialData Tutorial => this.tutorial;


    /// <summary>
    /// Call for new user or user start new event
    /// </summary>
    public void CreateUser(EventID _id)
    {
        this._id = _id;
        this.currentRewardIndex = 0;

        booster = new UserGameBoosterData();
        booster.CreateUser(_id);

        map = new UserGameMapData();
        map.CreateUser(_id);

        //cardChest = new UserGameCardChestData();
        //cardChest.CreateUser(_id);

        //delivery = new UserDeliveryDropData();
        //delivery.CreateUser();

        //wheel = new  UserGameWheelData();
        //wheel.CreateUser(_id);

        //store = new  UserGameStoreDatas();
        //store.CreateUser(_id);

        //tutorial = new UserGameTutorialData();
        //tutorial.CreateUser(_id);
    }

    public double OnUserOnline()
    {
        double totalOffline = GetTotalSecondOffline();

        //this.cardChest.OnUserOnline();
        //this.delivery.OnUserOnline(totalOffline);
        //this.wheel.OnUserOnline(totalOffline);
        //this.store.OnUserOnline(totalOffline);

        return totalOffline;
    }
    public void OnUserOffline()
    {
        SetTimeWhenUserOffline();

        //this.delivery.OnUserOffline();
        //this.wheel.OnUserOffline();
        //this.store.OnUserOffline();
    }
    private void SetTimeWhenUserOffline()
    {
        long currentTimeStamp = System.DateTime.Now.ToFileTime();
        SetTimeWhenUserNextMine();
        lastTimeOnlineTruly = currentTimeStamp;
    }
    private void SetTimeWhenUserNextMine()
    {
        long currentTimeStamp = System.DateTime.Now.ToFileTime();
        lastTimeOnline = currentTimeStamp;
    }
    public double GetTotalSecondOffline()
    {
        if (lastTimeOnline <= 0)
            return 0;

        System.DateTime data =  System.DateTime.FromFileTime(lastTimeOnline);
        if(data != null)
        {
            System.TimeSpan _timeSpanDIf = System.DateTime.Now.Subtract(data);
            return _timeSpanDIf.TotalSeconds;
        }
        return 0;
    }

    //public List<CardData> GoToNextRewardIndex()
    //{
    //    SetTimeWhenUserNextMine();

    //    currentRewardIndex++;

    //    List<CardData> listCardsCanFound = new List<CardData>();
    //    if (this._id == EventID.MOTHERLAND)
    //    {
    //        this.booster.GoToNextRewardIndex();
    //        this.map.GoToNextRewardIndex();
    //        this.delivery.GoToNextRewardIndex();
    //        listCardsCanFound = this.cardChest.GoToNextIndex(currentRewardIndex);
    //        this.store.GoToNextIndex(currentRewardIndex);
    //    }
    //    else
    //    {
    //        this.delivery.GoToNextRewardIndex();
    //        listCardsCanFound = this.cardChest.GoToNextIndex(currentRewardIndex);
    //        this.store.GoToNextIndex(currentRewardIndex);
    //    }

    //    return listCardsCanFound;
    //}
}

#region Booster
///     - GameBoosterData
///         + UserCoin
///         + UserElixir
///         + UserCard;
///         + User wheel slot spins
///         + User chest free, chest wait time
///         + Key, Crown
[System.Serializable]
public class UserGameBoosterData
{
    public EventID _id;
    //coin, elixir, key, crown
    public List<BoosterCommodity> boosters;

    public List<UserGameComradeBoosterData> _comrades;
    public double CurrentComradeIncomePerSec
    {
        get
        {
            if (this._comrades == null || _comrades.Count == 0)
                return 0;
            return _comrades.Sum(x => x.AmountComradePerSec);
        }
    }
    //ads x2 booster
    //public const double BOOST_TIME_ADD_EACH_WATCH = 10800; //3h
    //public const double MAX_BOOST_TIME_STOCK = 43200; //12h
    //public const double MIN_TIME_DIFF_FROM_MAX = 15 * 60; //chỉ khi time còn 11h45p trở xuống mới thoát tình trạng max

    //public double totalBoostTimeStock; //[0, MAX_BOOST_TIME_STOCK]
    //public bool IsCanClaimBoosting => totalBoostTimeStock < MAX_BOOST_TIME_STOCK- MIN_TIME_DIFF_FROM_MAX;
    //public bool IsInBoostingTime => totalBoostTimeStock > 0;

    ////will be assign by the handler when it init
    //private X2BoostingAdsHandler _adsBoostingHandler;
    //public void SetHandler(X2BoostingAdsHandler handler)
    //{
    //    this._adsBoostingHandler = handler;
    //}

    /// <summary>
    /// Call for new user or user start new event
    /// </summary>
    public void CreateUser(EventID _id)
    {
        this._id = _id;
        this.AddBooster(BoosterType.COMRADE, GameDefine.COMRADE_DEFAULT);
        this.AddBooster(BoosterType.SCIENCE, 0);
        this.AddBooster(BoosterType.GOLD, GameDefine.GOLD_DEFAULT);


        _comrades = new List<UserGameComradeBoosterData>();
        EventDataConfig _eventConfig = GameEventDataConfigs.Instance.GetConfig(this._id);
        this.AddBooster(_eventConfig.FirstGenerator._typeID, 1);

        foreach (IndustryConfig _industry in _eventConfig._industryConfigs)
        {
            UserGameComradeBoosterData com = new UserGameComradeBoosterData() { _industryID = _industry._typeID, _amountTimeBought = 0 };
            com.OnUserOnline(_industry, _eventConfig._initialComrade, _eventConfig._multiplierComrade);
            _comrades.Add(com);
        }
    }

    public UserGameBoosterData()
    {
        this.boosters = new List<BoosterCommodity>();
        this.dicCallBackValueBooster = new Dictionary<BoosterType, CallbackEventBooster>();
        this._comrades = new List<UserGameComradeBoosterData>();
    }

    #region Booster
    private Dictionary<BoosterType, CallbackEventBooster> dicCallBackValueBooster;

    public void AddCallbackBooster(BoosterType type, UnityAction<BoosterCommodity> callback)
    {
        if (this.dicCallBackValueBooster.ContainsKey(type))
        {
            this.dicCallBackValueBooster[type].AddListener(callback);
        }
        else
        {
            CallbackEventBooster unityEvent = new CallbackEventBooster();
            unityEvent.AddListener(callback);
            this.dicCallBackValueBooster.Add(type, unityEvent);
        }
        BoosterCommodity booster = GetBoosterCommodity(type);
        if (booster != null)
            callback?.Invoke(booster);
        //this.ChangeBoosterValue(booster.type, booster);
        else
        {
            callback?.Invoke(new BoosterCommodity(type, 0));

            //if (this.dicCallBackValueBooster.ContainsKey(type))
            //{
            //    this.dicCallBackValueBooster[type].Invoke(new BoosterCommodity(type, 0));
            //}
        }

    }

    public void RemoveCallbackBooster(BoosterType type, UnityAction<BoosterCommodity> callback)
    {
        if (this.dicCallBackValueBooster.ContainsKey(type))
        {
            this.dicCallBackValueBooster[type].RemoveListener(callback);
        }
    }
    // invoke callbacks
    public void ChangeBoosterValue(BoosterType type, BoosterCommodity booster)
    {
        if (this.dicCallBackValueBooster.ContainsKey(type))
        {
            this.dicCallBackValueBooster[type].Invoke(booster);
        }
    }
    public BoosterCommodity AddBooster(BoosterType type, double value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b == null)
        {
            b = new BoosterCommodity(type, value);
            this.boosters.Add(b);
        }
        return b;
    }
    public BoosterCommodity AddValueBooster(BoosterType type, double value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            b.Add(value);

            ChangeBoosterValue(type, b);
            return b;
        }
        else
        {
            b = this.AddBooster(type, value);
            ChangeBoosterValue(type, b);
            return b;
        }
    }
    public BoosterCommodity AddValueBooster(BoosterCommodity booster)
    {
        BoosterCommodity b = this.GetBoosterCommodity(booster.type);
        if (b != null)
        {
            b.Add(booster.GetValue());

            ChangeBoosterValue(b.type, b);

            return b;
        }
        else
        {
            b = this.AddBooster(booster.type, booster.GetValueDouble());
            ChangeBoosterValue(booster.type, b);
            return b;
        }
        return null;
    }
    public BoosterCommodity SetValueBooster(BoosterType type, double value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            b.Set(value);

            ChangeBoosterValue(type, b);

            return b;
        }
        return null;
    }
    public BoosterCommodity UseBooster(BoosterType type, double value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            if (b.Use(value))
            {
                ChangeBoosterValue(type, b);

                return b;
            }
        }
        return null;
    }
    public bool IsHasBooster(BoosterType b)
    {
        return this.boosters.Find(x => x.type == b) != null;
    }
    public bool IsHasBooster(BoosterType type, double value)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            return b.CanUse(value);
        }
        return false;
    }
    public double MaxBoosterDiv(BoosterType type, double valueDiv)
    {
        BoosterCommodity b = this.GetBoosterCommodity(type);
        if (b != null)
        {
            if (valueDiv == 0)
                return 0;

            return b.GetValueDouble() / valueDiv;
        }
        return 0;
    }
    public BoosterCommodity GetBoosterCommodity(BoosterType type)
    {
        return this.boosters.Find(x => x.type == type);
    }
    #endregion booster

    #region Ads Boosting X2 Profit
    public bool CompleteWatchAddBoosting()
    {
        //bool registerToIStatHandler = totalBoostTimeStock == 0;

        //totalBoostTimeStock = totalBoostTimeStock + BOOST_TIME_ADD_EACH_WATCH;
        //if (totalBoostTimeStock > MAX_BOOST_TIME_STOCK)
        //    totalBoostTimeStock = MAX_BOOST_TIME_STOCK;

        //return registerToIStatHandler;
        return false;
    }
    #endregion Ads Boosting X2 Profit

    #region Comrade
    public void SetUpComrade()
    {
        EventDataConfig _eventConfig = GameEventDataConfigs.Instance.GetConfig(this._id);

        foreach (IndustryConfig _industry in _eventConfig._industryConfigs)
        {
            UserGameComradeBoosterData _com = _comrades.Find(x => x._industryID == _industry._typeID);
            _com.OnUserOnline(_industry, _eventConfig._initialComrade, _eventConfig._multiplierComrade);
        }
    }
    public double AddComradeBySecond(double time = 1)
    {
        if(CurrentComradeIncomePerSec > 0)
        {
            this.AddValueBooster( BoosterType.COMRADE, CurrentComradeIncomePerSec);
        }
        return CurrentComradeIncomePerSec;
    }
    #endregion
    /// <summary>
    /// subtract the ofline time to data save
    /// return the boosting time in compare to totalOffline
    /// just like if user have 4h boosting, but offline 5h => return 4
    /// just like if user have 4h boosting, but offline 1h => return 1
    /// just like if user have 4h boosting, but offline 4h => return 4
    /// the profit offine = (value return *2 + offlinetime - value return) * profit per sec;
    /// </summary>on
    /// <param name="totalOffline"></param>
    /// <returns></returns>
    public double OnUserOnline(double totalOffline)
    {
        SetUpComrade();

        //double timeBoostingOffline = Math.Min(totalBoostTimeStock, totalOffline);
        ////subtract offline time to boosting time
        //totalBoostTimeStock -= totalOffline;
        //if (totalBoostTimeStock < 0)
        //    totalBoostTimeStock = 0;

        //return timeBoostingOffline;

        return 0;
    }
    public void GoToNextRewardIndex()
    {
        this.boosters.Find(x => x.type == BoosterType.COMRADE).Set(0);
        this._comrades.ForEach(x => x.GoNextRankIndex());
    }
}

[System.Serializable]
public class UserGameComradeBoosterData
{
    public BoosterType _industryID;
    public int _amountTimeBought;
    private double _multiplierComrade = 1000;
    private double _initialCost = 5000;
    public double ConfigMultiplierComrade => _multiplierComrade;

    public double ConfigInitialCostComrade => _initialCost;

    private double _comrateInitialConfig = 1;
    public double _comradeValueBooster = 1;

    private double _comrateStarting = 0;

    public double AmountComradePerSec => _amountTimeBought * _comradeValueBooster * _comrateInitialConfig + _comrateStarting;

    public BoosterCommodity _nextCost;

    public void OnUserOnline(IndustryConfig _industryConfig, double _initialCostComrade, double _multiplierComrade)
    {
        this._comrateInitialConfig = _industryConfig._comradeInitial;

        if (_industryConfig._firstIndustry)
            _comrateStarting = 1;

        _initialCost = _initialCostComrade;
        this._multiplierComrade = _multiplierComrade;

        if (_nextCost == null || _nextCost.type != this._industryID)
        {
            _nextCost = new BoosterCommodity(_industryID, _initialCostComrade);
        }
    }

    public void OnUpgradeComradeBought(int amountTimeBought = 1)
    {
        this._amountTimeBought += amountTimeBought;

        this._nextCost.Set(_initialCost * _multiplierComrade.Pow(this._amountTimeBought));
    }

    public double GetCostWithNextNStep(int step)
    {
        return _initialCost * _multiplierComrade.Pow(this._amountTimeBought + step);
    }

    public void GoNextRankIndex()
    {
        _amountTimeBought = 0;
        _comradeValueBooster = 1;
    }
}

#endregion Booster

#region Map
[System.Serializable]
public class GeneratorData
{
    /// <summary>
    /// Will be bind at the starting
    /// </summary>
    private GeneratorConfig _config;

    public BoosterType id;

    public double _amountGenerator;

    /// <summary>
    /// Mục tiêu science tiếp theo, up x10 mỗi lần
    /// </summary>
    public double _nextScienceObjective;

    /// <summary>
    /// Số lần achiven science object đang chờ claim
    /// </summary>
    public int _amountObjectiveCached;

    public double ScienceWaitingToBeCollect
    {
        get
        {
            if(this._config != null)
            {
                return this._config.sciencePerObjecttive * _amountObjectiveCached;
            }
            else
            {
                Debug.LogError("CONFIG GENERATOR IN DATA NULL, BIND AGAIN");
                return 0;
            }
        }
    }

    //Only for mine
    public double _timeLeftToProfit;

    public GeneratorData(BoosterType id, int initAmount = 0)
    {
        this.id = id;
        _amountGenerator = initAmount;
        _timeLeftToProfit = 0;

        _nextScienceObjective = 10;
        _amountObjectiveCached = 0;
    }
    public GeneratorData BindConfig(GeneratorConfig config)
    {
        this._config = config;
        return this;
    }

    public GeneratorData(GeneratorData data)
    {
        //standard data for all type
        this.id = data.id;
        this._amountGenerator = data._amountGenerator;
        this._timeLeftToProfit = data._timeLeftToProfit;

        this._nextScienceObjective = data._nextScienceObjective;
        this._amountObjectiveCached = data._amountObjectiveCached;
    }
    public double AchiveScienceObjectiveGoal()
    {
        _nextScienceObjective *= 10;
        _amountObjectiveCached++;

        return ScienceWaitingToBeCollect;
    }
}
[System.Serializable]
public class UserGameMapData
{
    public EventID _id;
    public List<GeneratorData> datas;
    public int cacheLastRewardIndex;
    public UserGameMapData()
    {
        datas = new List<GeneratorData>();
    }
    /// <summary>
    /// Call for new user or user start new event
    /// </summary>
    public void CreateUser(EventID _id)
    {
        this._id = _id;

        datas = new List<GeneratorData>() { new GeneratorData(GameEventDataConfigs.Instance.GetConfig(this._id).FirstGenerator._typeID, 1) };
        cacheLastRewardIndex = -1;
    }

    /// <summary>
    /// Lưu map hiện tại lại
    /// </summary>
    public void SaveData()
    {
        //clear previous data
        if (datas == null)
            datas = new List<GeneratorData>() { new GeneratorData(GameEventDataConfigs.Instance.GetConfig(this._id).FirstGenerator._typeID, 1) };
        else
            this.datas.Clear();

        //get all generator on the field
        List<BaseGenerator> _gens = GameSceneManager.Instance._generatorTabBase.AllGenerator;
        foreach (BaseGenerator _gen in _gens)
        {
            if(_gen.Data != null && _gen.Data._amountGenerator > 0)
                datas.Add(new GeneratorData(_gen.Data));
        }
    }

    public GeneratorData GetData(BoosterType _type)
    {
        if(datas != null && datas.Count > 0)
        {
            return datas.Find(x => x.id == _type);
        }
        return null;
    }

    public void GoToNextRewardIndex()
    {
        this.datas.Clear();
    }
}

# endregion Map

//#region Card and Chest 

//public enum GameCardState
//{
//    NOT_UNLOCK_AND_NOT_FOUNT,
//    CAN_FOUND_NOT_UNLOCK,
//    UNLOCKED_NOTMAX,
//    //MAX_LEVEL,
//}
//[System.Serializable]
//public class CardData
//{
//    public EventID eventID;
//    public int cardID;

//    public int currentCardOwned;

//    public int currentLevel;
//    public GameCardState state;

//    public int CurrentLevel => this.currentLevel;

//    public CardTierID Tier => this.CardUsageConfig.tier;
//    public bool IsUnlocked => state == GameCardState.UNLOCKED_NOTMAX;
//    public bool IsMaxLevel => currentLevel >= CardUsageConfig.maxLevel;

//    public int CurrentCardOwned => currentCardOwned;
//    public int NextCardRequired
//    {
//        get
//        {
//            List<int> config = EventDataConfigs.Instance.costCardUpgradeCard;
//            if (currentLevel == 0)
//                return 0;
//            if (this.currentLevel >= 1 && currentLevel <= config.Count)
//                return config[this.currentLevel-1];
//            return config[config.Count - 1] * (int)Mathf.Pow(2, currentLevel - config.Count + 1);
//        }
//    }
//    public double CostUpgradeNextLevel
//    {
//        get
//        {
//            return EventDataConfigs.Instance.GetElixirToNextUpgrade(this.CardUsageConfig.tier, this.currentLevel);
//        }
//    }

//    public bool IsEnoughCardToUpgrade => CurrentCardOwned >= NextCardRequired;
//    public bool IsEnoughElixirToUpgrade => IddleGameManager.Instance.GetValueBooster(BoosterType.SCIENCE).GetValueDouble() >= CostUpgradeNextLevel;

   
//    public bool IsCanUpgrade => IsEnoughCardToUpgrade && IsEnoughElixirToUpgrade;

//    //Todo: bool IsCanAutomate => check có thể upgrade đến level mine yêu cầu
//    public CardData()
//    {

//    }
//    public CardData(CardData c)
//    {
//        this.eventID = c.eventID;
//        this.cardID = c.cardID;
//        this.currentLevel = c.currentLevel;
//        this.state = c.state;
//        this.currentCardOwned = c.currentCardOwned;
//    }
//    public void BindConfig(EventCardUsageConfig _config, EventID _eventID, int inittialLevel = 0, GameCardState initialStat = GameCardState.NOT_UNLOCK_AND_NOT_FOUNT)
//    {
//        this.eventID = _eventID;
//        this.cardID = _config.idCard;
//        this.currentLevel = inittialLevel;
//        this.state = initialStat;
//        this.currentCardOwned = 0;
        
//    }

//    private EventDataConfig _eventConfig;
//    public EventDataConfig EventConfig
//    {
//        get
//        {
//            if (_eventConfig == null)
//                _eventConfig = EventDataConfigs.Instance.GetConfig(this.eventID);
//            return _eventConfig;
//        }
//    }

//    private EventCardUsageConfig _cardUsageConfig;
//    public EventCardUsageConfig CardUsageConfig
//    {
//        get
//        {
//            if (_cardUsageConfig == null)
//                _cardUsageConfig = EventConfig.GetCardUsageConfig(this.cardID);
//            return _cardUsageConfig;
//        }
//    }

//    public CardConfig CardConfig => this.CardUsageConfig.CardConfig;


//    public double CurrentBoostingValue
//    {
//        get
//        {
//            return CardUsageConfig.GetBoostingValueByLevel(currentLevel);
//        }
//    }
//    public double NextBoostingValue
//    {
//        get
//        {
//            return CardUsageConfig.GetBoostingValueByLevel(currentLevel + 1);
//        }
//    }

//    //Unlocked but user chưa click vào UI để xem
//    public bool isNew = false;

//    public void SetNewStatus(bool _isNew)
//    {
//        isNew = _isNew;
//    }

//    public void SetCanFound()
//    {
//        if(this.state == GameCardState.NOT_UNLOCK_AND_NOT_FOUNT)
//            this.state = GameCardState.CAN_FOUND_NOT_UNLOCK;
//        else
//        {
//            Debug.LogError($"This card state is {state} before");
//        }
//    }
//    public void Unlocked(int addCard = 0)
//    {
//        this.state = GameCardState.UNLOCKED_NOTMAX;
//        SetNewStatus(true);

//        this.currentCardOwned = addCard;
//        this.currentLevel = 1;
//    }
//    public void UpgradeCard()
//    {
//        if(this.state == GameCardState.UNLOCKED_NOTMAX && !this.IsMaxLevel)
//        {
//            //trừ card require
//            currentCardOwned -= NextCardRequired;

//            //cộng level lên 1
//            this.currentLevel++;
//        }

//    }


//    public int GetTotalCardRequireFromCurrentToLevel(int levelTo)
//    {
//        //Không support cho card chưa unlock
//        if (currentLevel == 0)
//        {
//            Debug.LogError($"HÀM NÀY KHÔNG HỖ TRỢ CARD CHƯA UNLOCK id {cardID} level {currentLevel} state {this.state}");
//            return 0;
//        }

//        if(levelTo <= this.currentLevel)
//        {
//            return 0;
//        }

//        if (levelTo == this.currentLevel + 1)
//            return NextCardRequired;

//        //list config tại vị trí i là số upgrade từ level i+1 leen6 level i+2: config[0] là cost upgrade từ level 1 lên level 2
//        //Vì ko có level 0

//        //Tổng require từ level 1 up lên level 4 = config[0] (lên lv 2) + config[1] (lên lv 3) + config[2] (lên lv 4)
//        //= config[currentLevel -1] + loop + config[levelTo - 2] {in loop}
//        List<int> config = EventDataConfigs.Instance.costCardUpgradeCard;
//        int result = 0;
//        for (int i = currentLevel - 1; i < levelTo-2; i++)
//        {
//            if(i < config.Count)
//                result += config[i];
//            else
//                result += config[config.Count - 1] * (int)Mathf.Pow(2, i - config.Count + 1);
//        }

//        return result;
//    }

//    public double GetTotalElixirRequireFromCurrentToLevel(int levelTo)
//    {
//        if (currentLevel == 0)
//        {
//            Debug.LogError($"HÀM NÀY KHÔNG HỖ TRỢ CARD CHƯA UNLOCK id {cardID} level {currentLevel} state {this.state}");
//            return 0;
//        }

//        if (levelTo <= this.currentLevel)
//            return 0;

//        if (levelTo == this.currentLevel + 1)
//            return CostUpgradeNextLevel;

//        return EventDataConfigs.Instance.GetTotalElixirToLevel(this.CardUsageConfig.tier, this.currentLevel, levelTo);
//    }

//    public bool IsEnoughCardAndResourceToLevel(int levelTo)
//    {
//        return this.currentCardOwned >= this.GetTotalCardRequireFromCurrentToLevel(levelTo) &&
//            IddleGameManager.Instance.IsHaveBooster(BoosterType.SCIENCE, GetTotalElixirRequireFromCurrentToLevel(levelTo));
//    }
//}

//[System.Serializable]
//public class UserGameCardChestData
//{
//    public List<CardData> cardDatas;
    
//    public int currentRewardIndex;

//    public Dictionary<GameCardState, List<CardData>> dicCardDatas; 

//    public UserGameCardChestData()
//    {
//        cardDatas = new List<CardData>();
//        dicCardDatas = new Dictionary<GameCardState, List<CardData>>();
//    }
//    public void CreateUser(EventID _id)
//    {
//        cardDatas = new List<CardData>();

//        this.currentRewardIndex = 0;
//        LoadDataInToCardList(_id);

//        LoadDataIntoDic();

//        GoToNextIndex(0);

//        //LoadSampleData();


//    }
//    public void OnUserOnline()
//    {
//        LoadDataIntoDic(true);
//    }
//#if UNITY_EDITOR
//    private void LoadSampleData()
//    {
//        AddCardToCanFound(GameCardID.MINE_FORGED);
//        AddCardToCanFound(GameCardID.MINE_AMETHYST);
//        UnlockCard(GameCardID.MINE_FORGED, 10);
//        UnlockCard(GameCardID.MINE_AMETHYST,1);

//        AddCardToCanFound(GameCardID.MINE_CITRINE);
//        AddCardToCanFound(GameCardID.MANAGER_AMETHYST);
//        AddCardToCanFound(GameCardID.MANAGER_CITRINE);

//        AddCardToCanFound(GameCardID.ROCK_PROFIT);
//        AddCardToCanFound(GameCardID.SPAWN_TIME);
//        AddCardToCanFound(GameCardID.MINE_PROFIT);

//        Debug.Log("index reward set to 5 as we testing");
//        this.currentRewardIndex = 5;

//        //DebugCardsData();
//    }
//    private void DebugCardsData()
//    {
//        foreach(CardData _card in this.cardDatas)
//        {
//            try
//            {
//                _card.DebugInfo();
//            }
//            catch (System.Exception e)
//            {

//                throw e;
//            }
//        }
//    }
//    private void DebugCard()
//    {

//    }
//#endif

//    private void LoadDataInToCardList(EventID _id)
//    {
//        //we get all the card in event usage
//        List<EventCardUsageConfig> _cardUsageConfigs = EventDataConfigs.Instance.GetConfig(_id)?.cardUsageConfigs;
//        if(_cardUsageConfigs != null)
//        {
//            //wea add all them to the data, but the status is locked, as our life
//            foreach(EventCardUsageConfig _config in _cardUsageConfigs)
//            {
//                CardData _data = new CardData();
//                _data.BindConfig(_config, _id, 0, GameCardState.NOT_UNLOCK_AND_NOT_FOUNT);

//                this.cardDatas.Add(_data);
//            }
//        }
//    }

//    public void LoadDataIntoDic(bool isClearDic = false)
//    {
//        if(dicCardDatas == null)
//            dicCardDatas = new Dictionary<GameCardState, List<CardData>>();

//        if (isClearDic)
//            dicCardDatas.Clear();

//        foreach (CardData item in cardDatas)
//        {
//            AddCardToListInDic(item.state, item);
//        }
//    }
//    private void AddCardToListInDic(GameCardState state, CardData _card)
//    {
//        if (dicCardDatas.ContainsKey(state))
//            dicCardDatas[state].Add(_card);
//        else
//            dicCardDatas.Add(state, new List<CardData>() { _card });
//    }
//    public CardData AddCardAmount(int _cardID, int addAmount)
//    {
//        CardData _card = GetCard(_cardID);
//        if (_card != null)
//        {
//            switch (_card.state)
//            {
//                case GameCardState.CAN_FOUND_NOT_UNLOCK:
//                    UnlockCard(_card.cardID, addAmount);
//                    break;
//                case GameCardState.UNLOCKED_NOTMAX:
//                    _card.currentCardOwned += addAmount;
//                    break;
//                case GameCardState.NOT_UNLOCK_AND_NOT_FOUNT:
//                    Debug.LogError("CARD NÀY CHƯA ĐẠT ĐẾN MINE UNLOCK, BREAK RA KO ADD VÀO");
//                    break;
//            }

//            //_card.DebugInfo();
//        }
//        return _card;
//    }
//    public CardData UnlockCard(int _cardID,int addCard = 0)
//    {
//        CardData _card = GetCard(_cardID);
//        if (_card != null)
//        {
//            _card.Unlocked(addCard);
//            _card.DebugInfo();

//            SwitchCardState(_cardID, GameCardState.CAN_FOUND_NOT_UNLOCK, GameCardState.UNLOCKED_NOTMAX);
//        }
//        return _card;
//    }
    
//    public CardData AddCardToCanFound(int _cardID)
//    {
//        CardData _card = GetCard(_cardID);
//        if (_card != null)
//        {
//            _card.SetCanFound();
//            //_card.DebugInfo();
//            SwitchCardState(_cardID, GameCardState.NOT_UNLOCK_AND_NOT_FOUNT, GameCardState.CAN_FOUND_NOT_UNLOCK);
//        }
//        return _card;
//    }

//    public void SwitchCardState(int _cardID, GameCardState _oldState, GameCardState _newState)
//    {
//        //tạm thời clear hết dic đi rồi add lại, 
//        //TODO: refactor this
//        //LoadDataIntoDic(true);

//        //tìm card với id trên trong oldstate
//        //dời nó qua newState

//        if (dicCardDatas.ContainsKey(_oldState))
//        {
//            List<CardData> olds = this.dicCardDatas[_oldState];
//            CardData _card = olds.Find(x => x.cardID == _cardID);
//            if (_card != null)
//            {
//                AddCardToListInDic(_newState, _card);

//                this.dicCardDatas[_oldState].Remove(_card);
//            }
//        }
//        else
//        {
//            LoadDataIntoDic(true);
//        }
//    }

//    public CardData UpgradeCard(int _cardID)
//    {
//        CardData _card = GetCard(_cardID);
//        if (_card != null)
//        {
//            _card.UpgradeCard();
            
//        }
//        return _card;
//    }

//    public const int MINE_INDEX_BEFORE_SHOW_TUT = 1;
//    public List<CardData> GoToNextIndex(int index)
//    {
//        this.currentRewardIndex = index;
//        //set card found new card

//        List<CardData> canFound = GetNewCanFoundCard(currentRewardIndex);
//        if(canFound != null && canFound.Count > 0)
//        {
//            foreach(CardData _card in canFound)
//            {
//                AddCardToCanFound(_card.cardID);
//            }
//        }

//        if (currentRewardIndex == MINE_INDEX_BEFORE_SHOW_TUT)
//        {
//            //khi next mine 1 new found tới 6 card
//            //nhưng lúc này chưa có tutorial về card => ko show trên UI
//            canFound.Clear();
//        }

//        return canFound;
//    }

//    public CardData GetCard(int _cardID)
//    {
//        return this.cardDatas.Find(x => x.cardID == _cardID);
//    }
    
//    public List<CardData> GetDataByState(GameCardState state)
//    {
//        if(dicCardDatas.ContainsKey(state))
//            return dicCardDatas[state];

//        return cardDatas.Where(x => x.state == state).ToList();
//    }
//    public List<CardData> GetDataByState(GameCardState state, CardTierID _tier)
//    {
//        if (dicCardDatas.ContainsKey(state))
//            return dicCardDatas[state].Where(x => x.Tier == _tier).ToList();

//        return cardDatas.Where(x => x.state == state && x.Tier == _tier).ToList();
//    }
//    public List<CardData> GetDataCanFoundAndUnlocked()
//    {
//        return cardDatas.Where(x => x.CardUsageConfig.indexMineUnlocking <= currentRewardIndex + 1).ToList();
//    }
//    public List<CardData> GetNewCanFoundCard(int indexReward)
//    {
//        //index mine unlocking của card là mine ui index
//        List<CardData> res = cardDatas.Where(x => x.CardUsageConfig.indexMineUnlocking == indexReward + 1).ToList();
//        return res;
//    }
//}
//#endregion Card and Chest 

//#region Delivery Drop

//[System.Serializable]
//public class UserDeliveryDropData
//{
//    public int currentIndex;


//    public const double TOTAL_TIME_OFFLINE_APPROVE_RESET = 60*60;
//    //Thời gian wave đã bắt đầu
//    public double timeWaveHaveStart;


//    public void ResetWave()
//    {
//        currentIndex = 0;
//        timeWaveHaveStart = 0;
//    }
//    public void OnClaimDrop()
//    {
//        currentIndex++;
//    }

//    public RewardSequenceConfig GetRewardData()
//    {

//        return EventDataConfigs.Instance.GetDeliveryRewardByIndex(this.currentIndex);
//    }
//    public RewardSequenceConfig GetRewardDataByType(DeliveryDropType type)
//    {
//        return EventDataConfigs.Instance.deliveryDropConfig._typeConfig.Find(x => x.type == type);
//    }
//    public void OnUserOffline()
//    {
//        timeWaveHaveStart = DeliveryDropHandler.Instance._totalTimePfWave;
//    }
//    public void OnUserOnline(double totalOffline)
//    {
//        if(totalOffline >= TOTAL_TIME_OFFLINE_APPROVE_RESET)
//        {
//            DeliveryDropHandler.Instance.ResetWave();
//        }

//        DeliveryDropHandler.Instance.LoadTimeOfWaveData(timeWaveHaveStart + totalOffline);
//    }

//    public void CreateUser()
//    {
//        ResetWave();
//    }

//    public void GoToNextRewardIndex()
//    {
//        ResetWave();
//    }
//}

//#endregion

//#region Wheel
//[System.Serializable]
//public class UserGameWheelData
//{
//    public EventID _id;
//    public int _spinsLeft;

//    //time for the free
//    public bool isCanSpinFree;
//    public const double TOTAL_TIME_WAIT_FOR_FREE = 86400; //24h
//    public long timeSpinFreeOld; //the last time we spin free

//    //time for the ads
//    public const int MAX_ADS_SPINS = 5;
//    public const double TOTAL_TIME_WAIT_FOR_ADS = 7200; //2h 7200
//    public long timeSpinAdsOld; //the last time we spin the first ads after full
//    public double totalTimeRemainToAddSpin; //total time have wait to add a spin

//    public bool IsMaxAdsSpin => this._spinsLeft == MAX_ADS_SPINS;
//    public bool IsCanSpin
//    {
//        get
//        {
//            //spin left > 0 || can spin free
//            return this._spinsLeft > 0 || IsCanSpinFree;
//        }
//    }
//    public bool IsCanSpinFree
//    {
//        get
//        {
//            return isCanSpinFree;
//        }
//    }

//    //will be assign by the handler when it init
//    private WheelHandler _wheelHandler;

//    private WheelEventUsageConfig _wheelConfig;
//    public WheelEventUsageConfig WheelConfig
//    {
//        get
//        {
//            if (_wheelConfig == null)
//                _wheelConfig = WheelConfigs.Instance.GetConfig(this._id);
//            return _wheelConfig;
//        }
//    }

//    public void CreateUser(EventID _id)
//    {
//        this._id = _id;

//        totalTimeRemainToAddSpin = TOTAL_TIME_WAIT_FOR_ADS;
//        Reload();
//    }

//    public void SetHandler(WheelHandler handler)
//    {
//        this._wheelHandler = handler;
//    }
//    public List<WheelRewardData> GetRewards()
//    {
//        return this.WheelConfig.GetRewards();
//    }
//    public WheelRewardData RandomReward()
//    {
//        return this.WheelConfig.RandomReward();
//    }

//    ///Call whenever user click spin button
//    ///Verify the spin slot again
//    ///If can, return the result spin
//    public WheelRewardData SpinWheel()
//    {
//        if (IsCanSpin)
//        {
//            if (IsCanSpinFree)
//            {
//                this.isCanSpinFree = false;
//                this.timeSpinFreeOld = DateTime.Now.ToFileTime();
//            }
//            else
//            {
//                if (IsMaxAdsSpin)
//                    this.timeSpinAdsOld = DateTime.Now.ToFileTime();

//                this._spinsLeft--;
//            }
//            return RandomReward();
//        }
//        return null;
//    }
//    public void OnUserOffline()
//    {
//        //save the from the wheel
//        totalTimeRemainToAddSpin = this._wheelHandler.TimeRemainForAds;
//    }
//    public void OnUserOnline(double totalOffline)
//    {
//        //Cái wheel handler có gọi hàm này 1 lần tại start nên gọi ở đây cũng vô nghĩa
//        //this.IsNewDay();

//        int totalSpinAdded = 0;

//        if(totalOffline >= totalTimeRemainToAddSpin)
//        {
//            totalOffline -= totalTimeRemainToAddSpin;

//            totalSpinAdded = 1;
//            totalTimeRemainToAddSpin = TOTAL_TIME_WAIT_FOR_ADS;
//        }

//        if(totalOffline > 0)
//        {
//            totalSpinAdded += (int)(totalOffline / TOTAL_TIME_WAIT_FOR_ADS);
//        }

//        if (totalSpinAdded > 0)
//            AddSpinAds(totalSpinAdded);
//    }

//    //Call this function when ever wheel dialog count enough 2h
//    public void AddSpinAds(int amountSpinsAdd)
//    {
//        _spinsLeft = Mathf.Clamp(_spinsLeft + amountSpinsAdd, 0, MAX_ADS_SPINS);
//    }


//    //we let the UI count the time, just like delivery handler and canon spawner
//    //this function only detect for the free spin
//    public bool IsNewDay()
//    {
//        //for the free
//        DateTime timeOldFT = DateTime.FromFileTime(this.timeSpinFreeOld);

//        if (timeOldFT == null)
//        {
//            //this.NewDay();
//            //this.timeSpinFreeOld = DateTime.Now.ToFileTime();

//            return true;
//        }

//        bool isDiffDate = timeOldFT.Date != DateTime.Now.Date;

//        //totalRemain = TOTAL_TIME_WAIT_FOR_FREE - DateTime.Now.TimeOfDay.TotalSeconds;
//        if (isDiffDate) //isMore24H || 
//        {
//            this.NewDay();

//            this.timeSpinFreeOld = DateTime.Now.ToFileTime();
//            return true;
//        }
//        return false;
//    }

//    public void NewDay()
//    {
//        Reload();
//    }
//    public void Reload()
//    {
//        this.isCanSpinFree = true;
//        this._spinsLeft = MAX_ADS_SPINS;


//    }
//}
//#endregion

//#region Shop

//[System.Serializable]
//public class UserGameStoreDatas
//{
//    public EventID _id;

//    //data of the package
//    public StorePackageData currentPackageData;
//    public StorePackageData CurrentPackageData
//    {
//        get
//        {
//            if (currentPackageData == null)
//                GeneratePackage();
//            return currentPackageData;
//        }
//    }

//    //======== CHEST ============
//    //data of the chest
//    public int currentChestIndex;

//    //time for the chest
//    public int _freeChestStock;
//    public const int MAX_CHEST_STOCK = 2;
//    public const double TOTAL_TIME_WAIT_FOR_FREE_CHEST = 7200; //2h 7200
//    public double totalTimeRemainToAddChest; //total time have wait to add a CHEST
//    public bool isCanClaimAdsChest;
//    //======== CHEST ============


//    //======== TIME SKIP ============
//    //public 
//    //======== TIME SKIP ============

//    public bool IsMaxFreeChest => this._freeChestStock == MAX_CHEST_STOCK;
//    public bool IsCanClaimFreeChest
//    {
//        get
//        {
//            return this._freeChestStock > 0;
//        }
//    }
//    public bool IsCanClaimAdsChest => isCanClaimAdsChest;
//    public bool IsCanClaimChest
//    {
//        get
//        {
//            return IsCanClaimFreeChest || IsCanClaimAdsChest;
//        }
//    }
//    public ChestID CurrentFreeChestID => this.StoreConfig.GetChestIDByIndex(this.currentChestIndex);
//    public StoreItemClaimType CurrentFreeChestClaimType
//    {
//        get
//        {
//            if (this.IsCanClaimFreeChest)
//                return StoreItemClaimType.FREE_CLAIM;
            
//            if(this.IsCanClaimAdsChest)
//                return StoreItemClaimType.ADS_CLAIM;

//            return StoreItemClaimType.NOT_READY;
//        }
//    }
//    //will be assign by the handler when it init
//    private StoreHandler _storeHandler;

//    private StoreEventConfig _storeConfig;
//    public StoreEventConfig StoreConfig
//    {
//        get
//        {
//            if (_storeConfig == null)
//                _storeConfig = StoreConfigs.Instance.GetConfig(this._id);
//            return _storeConfig;
//        }
//    }

//    public void CreateUser(EventID _id)
//    {
//        this._id = _id;

//        totalTimeRemainToAddChest = TOTAL_TIME_WAIT_FOR_FREE_CHEST;
//        _freeChestStock = MAX_CHEST_STOCK;

//        //GeneratePackage();
//    }
//    private void GeneratePackage(int currentRewardIndex)
//    {
//        StorePackageConfig _storePackageConfig = StoreConfig.GetRandomSpecialPackage();
//        this.currentPackageData = new StorePackageData().BindData(this._id, _storePackageConfig.id, StorePackageData.DropRandomCardReward(_storePackageConfig._rewardCardTier), currentRewardIndex);
//    }
//    public void GeneratePackage()
//    {
//        StorePackageConfig _storePackageConfig = StoreConfig.GetRandomSpecialPackage();
//        this.currentPackageData = new StorePackageData().BindData(this._id, _storePackageConfig.id, StorePackageData.DropRandomCardReward(_storePackageConfig._rewardCardTier), UserGameDatas.Instance.GetData(this._id).currentRewardIndex);
//    }
//    public void SetHandler(StoreHandler handler)
//    {
//        this._storeHandler = handler;
//    }

//    ///Call whenever user click spin button
//    ///Verify the spin slot again
//    ///If can, return the result spin
//    public ChestID ClaimChest()
//    {
//        if (IsCanClaimChest)
//        {
//            if (IsCanClaimFreeChest)
//            {
//                _freeChestStock--;
//                isCanClaimAdsChest = true;
//            }
//            else
//            {
//                isCanClaimAdsChest = false;
//            }
//            currentChestIndex++;

//            return StoreConfig.GetChestIDByIndex(currentChestIndex);
//        }
//        return  ChestID.NONE;
//    }

//    public void OnUserOffline()
//    {
//        //save the from the wheel
//        totalTimeRemainToAddChest = this._storeHandler.TimeRemainForChest;
//    }
//    public void OnUserOnline(double totalOffline)
//    {
//        //add time to package
//        if (this.currentPackageData != null)
//        {
//            this.currentPackageData.totalTimeWaitPackage += totalOffline;
//            if (currentPackageData.IsEnoughTime)
//                GeneratePackage();
//        }


//        //add time to chest
//        int totalChestAdded = 0;

//        if (totalOffline >= totalTimeRemainToAddChest)
//        {
//            totalOffline -= totalTimeRemainToAddChest;

//            totalChestAdded = 1;
//            totalTimeRemainToAddChest = TOTAL_TIME_WAIT_FOR_FREE_CHEST;
//        }

//        if (totalOffline > 0)
//        {
//            totalChestAdded += (int)(totalOffline / TOTAL_TIME_WAIT_FOR_FREE_CHEST);
//        }

//        if (totalChestAdded > 0)
//            AddFreeChest(totalChestAdded);

//        _storeHandler._currentPackageData = this.CurrentPackageData;
//        _storeHandler._timeHaveWaitForNewPackage = this.currentPackageData.totalTimeWaitPackage;
//        _storeHandler._timeRemainToWaitForFreeChet = this.totalTimeRemainToAddChest;
        

//    }

//    //Call this function when ever wheel dialog count enough 2h
//    public void AddFreeChest(int amountSpinsAdd)
//    {
//        _freeChestStock = Mathf.Clamp(_freeChestStock + amountSpinsAdd, 0, MAX_CHEST_STOCK);
//        isCanClaimAdsChest = true;
//    }

//    public void GoToNextIndex(int currentRewardIndex)
//    {
//        this.GeneratePackage(currentRewardIndex);
//    }
//}
//#endregion Shop

//#region Tutorial
//[System.Serializable]
//public class UserGameTutorialData
//{
//    public EventID _id;
//    public int currentTutorialLevelPack;
//    public int currentStepIndexInTutorialPack;

//    public int CurrentTutIndex => CurrentTutorialLevelPack;
//    public int CurrentStepIndex => currentStepIndexInTutorialPack;

//    public int CurrentTutorialLevelPack { get => currentTutorialLevelPack; set 
//        {
//            currentTutorialLevelPack = value;
//        } 
//    }

//    public UserGameTutorialData()
//    {

//    }
//    public void CreateUser(EventID _id)
//    {
//        this._id = _id;

//        switch (_id)
//        {
//            case EventID.NONE:
//                break;
//            case EventID.MOTHERLAND:
//                CurrentTutorialLevelPack = 100;
//                break;
//            case EventID.GEM_GALORE:
//                break;
//            case EventID.ELIXIR_DELUGE:
//                break;
//            case EventID.VOLCANO_ERUPTION:
//                break;
//            case EventID.INTO_ARCTIC:
//                break;
//            case EventID.JUNGLE_RUIN:
//                break;
//            case EventID.CANDY_FRENZY:
//                break;
//            case EventID.GGHOUL:
//                break;
//        }
//    }
//    public bool IsStartedTutorialIndex(int index)
//    {
//        return CurrentTutorialLevelPack >= index;
//    }
//    public bool IsCompletedTutorialIndex(int index)
//    {
//        return CurrentTutorialLevelPack > index;
//    }

//    public void CompleteTutorialIndex()
//    {
//        CurrentTutorialLevelPack++;
//        currentStepIndexInTutorialPack = 0;
//        //call save data thuần để đảm bảo data được lưu lại
//        GameDataManager.Instance.SaveUserData();
//    }

//    public void SkipTutoralIndex(int index)
//    {
//        CurrentTutorialLevelPack = index;
//        currentStepIndexInTutorialPack = 0;
//        //call save data thuần để đảm bảo data được lưu lại
//        GameDataManager.Instance.SaveUserData();
//    }

//    public void SetTutorialStep(int step)
//    {
//        CurrentTutorialLevelPack = step;
//        //call save data thuần để đảm bảo data được lưu lại
//        GameDataManager.Instance.SaveUserData();
//    }
//}
//#endregion Tutorial